import pytest
import os
import random
import string
import shutil
import alembic
import pytest_flask_sqlalchemy
import html5lib
import flask
import atexit
from glob import glob
from tree_set import TreeSet

# Causes email to be written to /tmp/lms42-email.txt
os.environ['SMTP_HOST'] = ''
os.environ["ALLOW_TILDE_LOGIN"] = "yes"

# Create a new data directory and database for each test session
# TODO: while the database uses rollbacks to reset between individual tests
# the rest of the data directory does not. It probably should, somehow.
data_dir = "tests/data/" + "".join([random.choice(string.ascii_lowercase) for i in range(8)])
os.environ["DATA_DIR"] = data_dir

# Always delete our private data directory when the main test process exits.
main_pid = os.getpid()
atexit.register(lambda: shutil.rmtree(data_dir) if os.getpid()==main_pid else None)


# Import lms42.app *after* we have set the environment variables.
from lms42.app import app, db

# Run all migrations
alembic_cfg = alembic.config.Config()
alembic_cfg.set_main_option('script_location', "migrations/")
alembic_cfg.set_main_option('sqlalchemy.url', app.config["SQLALCHEMY_DATABASE_URI"])
alembic.command.upgrade(alembic_cfg, 'head')
# with alembic.context.begin_transaction():
#     alembic.context.run_migrations()

# Create a html5 parser for validating all test results
html_parser = html5lib.HTMLParser(namespaceHTMLElements=False)

# Make sure the responses dir exists and is empty
os.makedirs("tests/responses", exist_ok=True)
for filename in glob("tests/responses/*"):
    os.unlink(filename)


class TestClient(flask.testing.FlaskClient):
    """A custom FlaskClient that adds:
    - Logging into the site with a single statement (`login`).
    - Checking that responses are valid HTML5.
    - Writing response documents to tests/responses/ for debugging.
    - And, by means of TreeSet, documented in `tree_set.py`:
        - Exposing the HTML5 tree from the last response (through `tree`).
        - Submitting a form within the last response (`TreeSet.submit`).
        - Following a link within the last response (`TreeSet.open`).
    """
    request_id = 0

    def open(self, *args, follow_redirects=True, as_tuple=False, **kwargs):
        environ, response = super().open(*args, follow_redirects=follow_redirects, as_tuple=True, **kwargs)

        self.tree = None
        self.last_path = None

        if isinstance(response, flask.wrappers.Response):

            if args and type(args[0])==str:
                method = kwargs.get('method', 'GET')
                TestClient.request_id += 1
                filename = f"tests/responses/{TestClient.request_id}.html"
                with open(filename, 'wb') as file:
                    file.write(response.data)
                print(method, args[0], kwargs.get('data', {}), "->", f"[{response.status_code}]", filename)

            if response.status_code not in [301,302,302]:
                self.last_path = environ['PATH_INFO']
                self.tree = TreeSet(self, html_parser.parse(response.data))
                for loc,code,args in html_parser.errors:
                    raise AssertionError(f"HTML5 error at {loc[0]}:{loc[1]}: {html5lib.constants.E[code] % args}")

        return (environ,response) if as_tuple else response

    def resolve_url(self, url):
        if url == "":
            url = self.last_path
        elif url != None and "://" not in url and not url.startswith("/"):
            path = self.last_path.split("/")
            path.pop()
            for part in url.split('/'):
                if part=='.':
                    pass
                elif part=='..':
                    if len(path)>0:
                        path.pop()
                else:
                    path.append(part)
            url = path.join('/')
        return url

    def login(self, name):
        response = self.post("/user/login", data=dict(
            email=f"~{name}@example.com",
        ), follow_redirects=True)

        assert b"User profile" in response.data

    def find(self, *args, **kwargs):
        return self.tree.find(*args, **kwargs)
        

app.test_client_class = TestClient


@pytest.fixture(scope='session', autouse=True)
def test_app():
    """This autouse fixture fills the (freshly created) database with
    a couple of user accounts (admin@example.com, student[1-5]@example.com
    and teacher[12]@example.com)."""
    app.config.update({
        "TESTING": True,
        "WTF_CSRF_CHECK_DEFAULT": False,
        "WTF_CSRF_ENABLED": False,
    })

    client = app.test_client()

    response = client.post("/user/login", data=dict(
        email="admin@example.com",
    ), follow_redirects=True)

    assert b"User profile" in response.data

    for nr in range(5):
        response = client.post("/people/new", data={
            "first_name": f"Student{nr}",
            "last_name": "",
            "email": f"student{nr}@example.com",
            "short_name": f"student{nr}",
            "class_name": "a",
            "level": 10,
            "is_active": "y",
        }, follow_redirects=True)

        assert b"Impersonate" in response.data # Verify that we landed on the user's profile page

    for nr in range(2):
        response = client.post("/people/new", data={
            "first_name": f"Teacher{nr}",
            "last_name": "",
            "email": f"teacher{nr}@example.com",
            "short_name": f"teacher{nr}",
            "class_name": "",
            "level": 50,
            "is_active": "y",
        }, follow_redirects=True)

        assert b"Impersonate" in response.data # Verify that we landed on the user's profile page

    return app


@pytest.fixture()
def _db(scope='session'):
    """Required by pytest-flask-sqlalchemy."""
    return db


@pytest.fixture(autouse=True)
def enable_transactional_test(db_session):
    """Have pytest-flask-sqlalchemy run each test within a transaction, which is
    reverted at the end."""
    pass


@pytest.fixture()
def client(test_app):
    """Fixture that allows tests to simulate requests to a fake lms42 server."""
    return test_app.test_client()
