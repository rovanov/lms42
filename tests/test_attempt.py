def test_submit(client):
    client.login("student1")
    client.open('/curriculum/vars')
    client.find("input.button", value="Start attempt anyway").submit()

    client.find(text="In progress!").require()
    client.find(in_text="You haven't uploaded any work yet.").require()
    client.find("input.button.is-warning", value='Submit attempt ignoring warnings').require()
