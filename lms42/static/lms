#!/usr/bin/env python3

from configparser import ConfigParser
from pathlib import Path
from glob import glob
import argparse
import json
import os
import pathlib
import secrets
import socket
import sys
import urllib.request
import subprocess
import shlex
from sys import platform

BASE_URL = os.environ.get('LMS_BASE_URL') or 'https://sd42.nl'
API_VERSION = 6
FALLBACK = ["code .", "vscode .", "codium .",]  # The default ides for opening a template

def run_ide(out_dir):
    """Helper function for running ides in the shell. 
    
    This function checks if the template contains a `.lms-ide` file (containing ide commands) and if so, reads it. 
    Any commands found will be put into a list; when no commands are found an empty list will be created.
    This list will be extended with the default ide commands as specified in `FALLBACK`.
    Note that the commands in the file wil take precedence over the fallback commands.

    This function then tries to execute the commands by order of index and will keep trying until exit-code 0 is
    returned by the application. If no command is successfully executed, an error message will be printed.

    Args:
     out_dir (string): The directory to be appended to the command.

    Returns:
     never
    """

    os.chdir(out_dir)

    try:
        commands = open(".lms-ide", "r").read().splitlines()
    except:
        commands = []
    
    commands += FALLBACK

    for command in commands:
        command = shlex.split(command)
        try:
            os.execvp(command[0], command)
        except:
            pass

    print("Error: Could not execute any of", commands)
    sys.exit(1)


class LmsCli:
    def __init__(self, args):
        self.migrate_groups()

        parser = argparse.ArgumentParser("lms")
        parser.add_argument('-v', '--verbose', default=0, action='count', help="enable verbose output")
        subparsers = parser.add_subparsers(title="command", dest="command", required=True)
        
        install_parser = subparsers.add_parser("install", help="install or upgrade lms")
        install_parser.add_argument('--finish', action="store_true", help=argparse.SUPPRESS)

        login_parser = subparsers.add_parser("login", help="connect to your sd42.nl account")

        template_parser = subparsers.add_parser("template", help="download the current assignment template")

        upload_parser = subparsers.add_parser("upload", help="upload your work for the current assignment")
        download_parser = subparsers.add_parser("download", help="download your last-submitted work for the current assignment")
        download_parser.add_argument("spec", help="the node id optionally followed by a '~' and the attempt number, eg vars~2")

        grade_parser = subparsers.add_parser("grade", help="teachers only: download everything needed for grading a student attempt")
        grade_parser.add_argument("spec", help="the student's short name optionally followed by '@' the node id and '~' attempt number, eg frank@vars~2")
        #grade_parser.add_argument('--open', action="store_true", help="open the work in your IDE")

        open_parser = subparsers.add_parser("open", help="open the current assignment in your IDE")
        #login_parser.add_argument("something", choices=['a1', 'a2'])

        self.opts = parser.parse_args(args)
        
        if self.opts.verbose >= 2:
            print("Arguments:", self.opts)
        
        self.config_path = os.path.join(os.environ['HOME'],'.config', 'lms.ini')
        self.config = ConfigParser()
        if pathlib.Path(self.config_path).is_file():
            self.config.read(self.config_path)
        if self.opts.verbose >= 2:
            print("Config:", {section: dict(self.config[section]) for section in self.config.sections()})

        getattr(self, "handle_"+self.opts.command.replace('-','_'))()


    def handle_install(self):
        if self.opts.finish:
            # This is the new version being called immediately post install
            print(f"Successfully installed version {API_VERSION} to {__file__}")
        else:
            path = self.update()
            os.execv(path, ["lms", "install", "--finish"])


    def update(self):
        bin_dir = os.path.join(os.environ['HOME'],'.local','bin')
        lms_path = os.path.join(bin_dir,"lms")
        pathlib.Path(bin_dir).mkdir(parents=True, exist_ok=True)

        with urllib.request.urlopen(BASE_URL+'/static/lms') as response:
            file = os.open(lms_path, os.O_CREAT | os.O_WRONLY | os.O_TRUNC, 0o755)
            os.write(file, response.read())
            os.close(file)

        return lms_path


    def handle_login(self):
        # Generate a random token and save it to our config file
        token = secrets.token_urlsafe()
        if 'auth' not in self.config:
            self.config.add_section('auth')
        self.config.set('auth', 'token', token)
        with open(self.config_path, 'w') as file:
            self.config.write(file)
        
        # Ask the user to authorize the token
        host = socket.gethostname()
        url = f"{BASE_URL}/api/authorize?host={urllib.parse.quote(host, safe='')}&token={token}"
        print(f"Go to this URL to authorize lms: {url}")
        os.system(f"xdg-open '{url}' 2> /dev/null")


    def request(self, path, data=None, return_response=False):
        # Combine BASE_URL, path and the API version number to a URL
        url = f"{BASE_URL}{path}{'&' if '?' in path else '?'}v={API_VERSION}"
        req = urllib.request.Request(url)
        req.add_header('authorization', self.config.get("auth", "token"))

        try:
            response = urllib.request.urlopen(req, data)
        except urllib.error.HTTPError as e:
            if e.code == 418: # I'm a teapot: our client is outdated
                # Upgrade the client
                self.update()
                print(f"Auto-upgraded lms client")
                # And call the new version with the same arguments
                os.execvp(sys.executable, [sys.executable] + sys.argv)
                self.die("Client upgrade failed")

            self.die(f"HTTP response code {e.code} requesting {path}:\n{e.read().decode()}")

        if return_response:
            return response

        content = json.loads(response.read())
        response.close()
        if isinstance(content,dict) and "error" in content:
            self.die(content.get("message") or content.get("error"))
        return content


    def get_current_attempt(self):
        attempt = self.request("/api/attempts/current")
        if not attempt:
            self.die("no current assignment")
        return attempt


    def get_attempt_dir(self, attempt):
        return os.path.join(os.environ['HOME'], 'lms', attempt['period'], attempt['module_id'], attempt['node_id'])


    def handle_template(self):
        attempt = self.get_current_attempt()
        out_dir = self.get_attempt_dir(attempt)

        if not self.download_template(attempt, out_dir):
            self.die(f"output directory {out_dir} already exists. Delete it first if you want to start with a fresh template.")


    def download_template(self, attempt, out_dir):
        try:
            os.makedirs(out_dir)
        except OSError:
            return False

        self.download_tgz(f"/api/attempts/{attempt['attempt_id']}/template", out_dir)

        print(f"Created {out_dir}")
        return True


    """Download and extract a gzipped tar archive from `path` and extract it into
    the `out_dir` directory, which must already exist."""
    def download_tgz(self, path, out_dir):
        response = self.request(path, return_response=True)

        tar = subprocess.Popen(['tar', 'xzC', out_dir], stdin=subprocess.PIPE)
    
        while True:
            chunk = response.read(64*1024)
            if not chunk:
                break
            tar.stdin.write(chunk)
        tar.stdin.close()
        tar.wait()


    def handle_download(self):
        attempts = self.request(f"/api/attempts/@{self.opts.spec.replace('~',':')}")
        if not attempts:
            self.die("no attempt found")
        attempt = attempts[0]
        out_dir = self.get_attempt_dir(attempt)

        try:
            os.makedirs(out_dir)
        except OSError:
            self.die(f"output directory {out_dir} already exists. Delete it first if you want to download your submitted work.")

        self.download_tgz(f"/api/attempts/{attempt['spec']}/submission", out_dir)
        print(f"Created {out_dir}")


    def handle_grade(self):
        attempts = self.request(f"/api/attempts/{self.opts.spec.replace('~',':')}")
        if not attempts:
            self.die("no attempt found")
        attempt = attempts[0]

        out_dir = os.path.join(os.environ['HOME'], 'lms', 'grading', attempt['spec'].replace(':','~'))

        # Delete the target directory if it is empty
        try:
            os.unlink(out_dir)
        except OSError:
            pass

        # Download the solution to the target direction, if it doesn't exist
        try:
            os.makedirs(out_dir)
        except OSError:
            print(f"Submission already exists in {out_dir}")
        else:
            self.download_tgz(f"/api/attempts/{attempt['spec']}/submission", out_dir)
            print(f"Downloaded to {out_dir}")

        # Symlink our local solution and node directories (if any) 
        for name in ["_node", "_solution", "_template"]:
            try:
                os.unlink(out_dir+"/"+name)
            except OSError:
                pass
        curriculum_dir = os.path.expanduser(self.config.get("grade", "curriculum_directory", fallback="~/curriculum"))
        glob_path = f"{curriculum_dir}/{attempt['period']}/{attempt['module_id']}/[0-9][0-9]-{attempt['node_id']}"
        node_dir = (glob(glob_path) or [None])[0]
        if node_dir:
            os.symlink(node_dir, out_dir+"/_node")
            for what in ["solution", "template"]:
                what_dir = f"{node_dir}/{what}{attempt['variant_id']}"
                if os.path.isdir(what_dir):
                    os.symlink(what_dir, f"{out_dir}/_{what}")

        run_ide(out_dir)


    def handle_upload(self):
        attempt = self.get_current_attempt()
        out_dir = self.get_attempt_dir(attempt)

        if not Path(out_dir).is_dir():
            self.die(f"assignment directory {out_dir} does not exist")

        cmd = 'gtar' if platform == 'darwin' else 'tar'
        try:
            tar = subprocess.Popen([cmd, 'czC', out_dir, '--exclude-backups', '--exclude-ignore=.gitignore', '--exclude-ignore=.lmsignore', '.'], stdout=subprocess.PIPE)
        except FileNotFoundError:
            print(f"Command not found: '{cmd}'.")
            if platform == 'darwin':
                print("Please install gnu-tar (using brew for instance).")
            sys.exit(1)
    
        result = self.request(f"/api/attempts/{attempt['attempt_id']}/submission", data=tar.stdout)
        tar.wait()
        
        print(f"Upload complete: {result['transferred']//1024}kb transferred")
        print(f"Please remember that you still need to submit in the web interface")


    def handle_open(self):
        attempt = self.get_current_attempt()
        out_dir = self.get_attempt_dir(attempt)
        self.download_template(attempt, out_dir)
        run_ide(out_dir)


    def die(self, msg):
        print(f"Error: {msg}", file = sys.stderr)
        sys.exit(1)


    def migrate_groups(self):
        base = os.path.join(os.environ['HOME'], 'lms')
        for src, dst in {"1.0": "1.1a", "1.1": "1.1b"}.items():
            if os.path.isdir(f"{base}/{src}"):
                print(f"Moving {src} to {dst}")
                os.system(f'mkdir -p "{base}/{dst}" ; cp -rab "{base}"/{src}/* "{base}/{dst}" && rm -rf "{base}/{src}"')


if __name__ == '__main__':
    LmsCli(sys.argv[1:])
