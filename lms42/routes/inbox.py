from ..app import db, app, get_base_url
from ..assignment import Assignment
from ..models import curriculum
from ..models.attempt import Attempt, Grading
from ..models.user import User
from ..models.attempt import get_notifications
from ..utils import role_required, utc_to_display
from copy import deepcopy
from flask_login import login_required, current_user
import flask
import io
import re
import random
import sqlalchemy as sa
import subprocess
import urllib
import datetime


@app.route('/inbox', methods=['GET'])
@role_required('teacher')
def grading_get_list():
    ungraded_attempts = Attempt.query \
        .join(Attempt.student) \
        .filter(sa.or_(
            Attempt.status.in_(("needs_grading","needs_consent","awaiting_approval",)),
            sa.and_(Attempt.status == "in_progress", Attempt.deadline_time < datetime.datetime.utcnow())
        )) \
        .filter(User.level == 10) \
        .order_by(User.class_name, Attempt.submit_time, Attempt.start_time)

    return flask.render_template('inbox.html',
        ungraded_attempts = ungraded_attempts,
        nodes = curriculum.get('nodes_by_id'),
        modules = curriculum.get('modules_by_id'),
    )


@app.route('/inbox/grade/<attempt_id>', methods=['POST'])
@role_required('teacher')
def grading_post(attempt_id):
    form = flask.request.form

    attempt = Attempt.query.get(attempt_id)

    if attempt.status == "in_progress":
        flask.flash("Attempt is still in progress!?")
        return flask.redirect("/inbox")
    if attempt.status != "needs_grading":
        flask.flash("Attempt was already graded! Your grading has replaced the old grading, but the old grading is still available in the database.")

    ao = Assignment.load_from_directory(attempt.directory)
    scores = ao.form_to_scores(form)
    motivations = ao.form_to_motivations(form)

    grade, passed = ao.calculate_grade(scores)

    # All exams with 5, 6 and 10 grades need approval, as well as a random 10% of the rest.
    needs_consent = "ects" in ao.node and (grade in [5,6,10] or random.randrange(0,10)==0 or bool(form.get('request_consent')))
 
    grading = Grading(
        attempt_id = attempt.id,
        grader_id = current_user.id,
        objective_scores = scores,
        objective_motivations = motivations,
        grade = grade,
        grade_motivation = form.get('motivation'),
        passed = passed,
        needs_consent = needs_consent,
    )

    if needs_consent:
        attempt.status = "needs_consent"
    elif "ects" in ao.node:
        attempt.status = "passed" if passed else "failed"
    elif form.get('formative_action') in ["failed","passed","repair"]:
        attempt.status = form.get('formative_action')
    else:
        attempt.status = "passed" if passed else "repair"

    db.session.add(grading)
    db.session.commit()

    attempt.write_json()

    student = attempt.student
    if attempt.status == "passed":
        flask.flash(f"{student.full_name} passed with a {round(grade)}!")
    elif attempt.status == "needs_consent":
        flask.flash("Grading needs consent from another teacher.")
    elif grade == None:
        flask.flash(f"{student.full_name} has not received a grade.")
    else:
        flask.flash(f"{student.full_name} did NOT pass with a {round(grade)}.")

    if "ects" in ao.node and attempt.status != "needs_consent":
        flask.flash(f"Remember to publish the grade in Bison! Don't finalize it yet. Work submitted on {utc_to_display(attempt.submit_time)}.")

    return flask.redirect(f"/people/{attempt.student_id}#attempts")

