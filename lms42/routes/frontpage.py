from ..app import db, app
import flask
from flask_login import login_required, current_user


@app.route('/', methods=['GET'])
def frontpage():
    return flask.render_template('frontpage.html')


@app.route('/coc', methods=['GET'])
@login_required
def coc():
    return flask.render_template('coc.html')

@app.route('/python-cheat-sheet', methods=['GET'])
def python_cheat_sheet():
    with open('lms42/templates/python-cheat-sheet.md') as file:
        markdown = file.read()
    return flask.render_template('markdown.html', title="Python cheat sheet", body=markdown)
