from ..app import app
from webpreview import web_preview
import flask
import os
import re
import urllib


PREVIEWS_DIR = f"{os.getcwd()}/{app.config['DATA_DIR']}/link_previews"
os.makedirs(PREVIEWS_DIR, exist_ok=True)


@app.route('/link_preview/<path:quoted_url>', methods=['GET'])
def get_link_preview(quoted_url):
    url = urllib.parse.unquote(quoted_url)
    quoted_url = urllib.parse.quote(url, safe='') # make sure its quoted, to use in a path

    preview_file = f"{PREVIEWS_DIR}/{quoted_url}"
    if not os.path.isfile(preview_file):
        img_url = None
        success = False
        if re.match(r'^https?://(www.)?youtube.com/', url):
            try:
                preview = web_preview(url, timeout=1000)
                img_url = preview[2]
                if img_url:
                    urllib.request.urlretrieve(img_url, preview_file)
                    success = True

            except Exception as e:
                print("link_preview web_preview failed:", e)

        if not success:
            try:
                img_url = f"https://api.thumbnail.ws/api/aba89ad67def69da2e0dead46cc67bc0677e63e1b432/thumbnail/get?url={quoted_url}&width=240"
                urllib.request.urlretrieve(img_url, preview_file)
            except Exception as e:
                print("link_preview thumbnail.ws failed", e)
                preview_file = os.getcwd()+"/lms42/static/404.jpg"
    
    return flask.send_file(preview_file, cache_timeout=3*86400)
