from urllib.parse import urlparse, urljoin
import wtforms as wtf
import wtforms.validators as wtv
import flask_wtf
import flask_login
import flask
import re
import datetime
import os

from ..app import db, app, get_base_url
from ..models.user import User, LoginLink
from ..email import send as send_email
from ..utils import generate_password, role_required


class LoginForm(flask_wtf.FlaskForm):
    email = wtf.StringField('Email', filters=[lambda email: email.strip() if email else email], 
            validators=[wtv.Email(), wtv.DataRequired()])
    submit = wtf.SubmitField('Send login link')


def is_safe_url(target):
    ref_url = urlparse(get_base_url())
    test_url = urlparse(urljoin(get_base_url(), target))
    return test_url.scheme in ('http', 'https') and ref_url.netloc == test_url.netloc


@app.route('/user/login', methods=['GET','POST'])
def login():
    form = LoginForm()
    if form.validate_on_submit():
        immediate_login = False
        email = form.email.data
        if email and email[0]=='~' and os.environ.get('ALLOW_TILDE_LOGIN')=='yes':
            email = email[1:]
            immediate_login = True
        user = db.session.query(User).filter_by(email=email).first()
        
        if user==None and User.query.get(1) == None:
            # Create the admin user with this email address
            user = User()
            user.id = 1
            user.first_name = 'Administrator'
            user.last_name = ''
            user.short_name = 'admin'
            user.level = 90
            user.email = email
            db.session.add(user)
            db.session.commit()
            immediate_login = True

        if user and user.level > 0:
            redirect_url = flask.request.args.get('next')
            if redirect_url and not is_safe_url(redirect_url):
                redirect_url = None

            if immediate_login:
                flask_login.login_user(user)
                return flask.redirect(redirect_url if redirect_url else "/curriculum")
                
            link = LoginLink()
            link.user = user
            link.secret = generate_password()
            link.redirect_url = redirect_url

            db.session.add(link)
            db.session.commit()

            url = flask.request.url_root + f"user/link/{link.id}/{link.secret}"

            send_email(email, "Your login link", f"""
Hi {user.first_name},

You can use this link to sign into LMS42:

{url}

Best,
Admin.""")

            flask.flash(f"A login link has been sent to: {email}.")
            return flask.render_template('layout.html', title="Link sent")

        flask.flash("This address is not registered.")
    return flask.render_template('generic-form.html', form=form, action="/user/login", title="Log in")


@app.route('/user/link/<id>/<secret>', methods=['GET'])
def login_link(id, secret):
    #if flask.request.method == "HEAD" or flask.request.headers.get("Sec-Fetch-User") == "?1":
    #    # This is a (Outlook safe link) preview - we should ignore it.
    #    flask.flash("Ignoring preview of login link")
    #    return flask.redirect("/") 
    
    # Expire old tokens
    LoginLink.query.filter(LoginLink.time <  datetime.datetime.utcnow()+datetime.timedelta(hours=-1)).delete()
    db.session.commit()

    # Load and verify this token
    link = LoginLink.query.get(id)
    if link != None and link.secret == secret:
        #db.session.delete(link)
        flask_login.login_user(link.user)
        #db.session.commit()
        return flask.redirect(link.redirect_url if link.redirect_url else "/curriculum")
    else:
        flask.flash("The login link has expired (1 hour) or has already been used.")
        return flask.redirect("/user/login")


@app.route('/user/logout', methods=["POST"])
def logout():
    flask_login.logout_user()
    return flask.redirect('/')


@app.route('/user/impersonate/<short_name>', methods=["POST"])
@role_required('admin')
def impersonate(short_name):
    user = User.query.filter_by(short_name = short_name).one()
    flask_login.login_user(user)
    return flask.redirect('/curriculum')


@app.route('/user/report_absent/<short_name>', methods=["POST"])
@role_required('teacher')
def report_absent(short_name):
    user = User.query.filter_by(short_name = short_name).one()
    user.report_absent(flask.request.form.get('reason'))
    db.session.commit()
    return flask.redirect('/people')
