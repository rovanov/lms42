import time
import os
from ..app import app
from ..models.user import User

if os.environ.get('FLASK_ENV','').startswith('dev'):

    @app.route('/test/ok', methods=['GET'])
    def ok():
        User.query.get(1)
        return "Ok"


    @app.route('/test/stall_transaction', methods=['GET'])
    def stall_transaction():
        User.query.get(1)
        time.sleep(3)
        User.query.get(2)
        return "Ok"
