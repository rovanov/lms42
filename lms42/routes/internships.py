"""Provides routes for the internship overview system."""

import os
import random
import string

import flask
from flask_login import login_required
from werkzeug.datastructures import FileStorage

from ..app import app, db
from ..forms.internship import AddTeamForm, EditCompanyForm, EditTeamForm
from ..models.internship import Company, Team
from ..utils import role_required


LOGO_DIR = app.config['DATA_DIR'] + '/logos'
os.makedirs(LOGO_DIR, exist_ok=True)


def save_logo(company, form):
    """Save the logo from the form into the filesystem.

    Args:
        company (Company): The Company object that the logo belongs to.
        form (FlaskForm): The Form object that contains the file in its 'logo'.

    Returns:
        str: The filename that the logo was saved with.
    """

    if isinstance(form.logo.data, FileStorage):
        logo_file = form.logo.data
        rand_str = ''.join(random.choices(string.ascii_uppercase + string.digits, k=10))
        filename = f'{rand_str}.png'

        logo_file.save(os.path.join(LOGO_DIR, filename))

        return filename

    return company.logo


@app.route('/internships', methods=['GET', 'POST'])
@login_required
def show_internships():
    companies = Company.query.order_by(Company.name).all()
    return flask.render_template('internships.html', companies=companies)


@app.route('/internships/<int:company_id>')
@login_required
def show_company(company_id):
    company = Company.query.get(company_id)
    return flask.render_template('company.html', company=company)


@app.route('/internships/add', methods=['GET', 'POST'])
@role_required('teacher')
def add_company():
    company_form = EditCompanyForm()

    if not company_form.validate_on_submit():
        return flask.render_template(
            'internships.html',
            form=company_form,
            )

    company = Company()
    company_form.populate_obj(company)
    company.logo = save_logo(company, company_form)

    db.session.add(company)
    db.session.commit()

    return flask.redirect(flask.url_for('show_internships'))


@app.route('/internships/edit/<int:company_id>', methods=['GET', 'POST'])
@role_required('teacher')
def edit_company(company_id):
    company = Company.query.get(company_id)

    company_form = EditCompanyForm(obj=company)

    if not company_form.validate_on_submit():
        return flask.render_template(
            'company.html',
            form=company_form,
            company=company,
            )

    company_form.populate_obj(company)
    company.logo = save_logo(company, company_form)

    db.session.commit()
    flask.flash(f'{company.name} was successfully edited.')

    return flask.redirect(
        flask.url_for(
            'show_company',
            company_id=company_id,
            ),
        )


@app.route('/internships/delete/<int:company_id>', methods=['POST'])
@role_required('teacher')
def delete_company(company_id):
    company = Company.query.get(company_id)

    db.session.delete(company)
    db.session.commit()

    flask.flash(f'{company.name} was successfully deleted.')

    return flask.redirect(flask.url_for('show_internships'))


@app.route('/internships/<int:company_id>/add_team', methods=['GET', 'POST'])
@role_required('teacher')
def add_team(company_id):
    company = Company.query.get(company_id)

    team_form = AddTeamForm()

    if not team_form.validate_on_submit():
        return flask.render_template(
            'company.html',
            form=team_form,
            company=company,
            )

    team = Team()
    team_form.populate_obj(team)
    team.company_id = company.id
    db.session.add(team)
    db.session.commit()

    return flask.redirect(
        flask.url_for(
            'show_company',
            company_id=company.id,
            ),
        )


@app.route('/internships/<int:team_id>/edit_team', methods=['GET', 'POST'])
@role_required('teacher')
def edit_team(team_id):
    team = Team.query.get(team_id)

    team_form = EditTeamForm(obj=team)

    if not team_form.validate_on_submit():
        return flask.render_template(
            'company.html',
            form=team_form,
            team=team,
        )

    team_form.populate_obj(team)
    db.session.commit()

    return flask.redirect(
        flask.url_for(
            'show_company',
            company_id=team.company.id,
            ),
        )


@app.route('/internships/<int:team_id>/delete', methods=['GET', 'POST'])
@role_required('teacher')
def delete_team(team_id):
    team = Team.query.get(team_id)
    team_name = team.name
    company_id = team.company.id

    db.session.delete(team)
    db.session.commit()
    flask.flash(f'{team_name} was successfully deleted.')

    return flask.redirect(
        flask.url_for(
            'show_company',
            company_id=company_id,
            ),
        )
