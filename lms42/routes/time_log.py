from ..app import db, app
import flask
from flask_login import current_user
import datetime
import sqlalchemy

TIME_LOG_QUERY = sqlalchemy.text("""
insert into time_log(user_id, date, start_time, end_time)
values(:user_id, :date, :time, :time)
on conflict (user_id, date)
do update set end_time = :time
""")

SUSPICIOUS_QUERY = sqlalchemy.text("""
insert into time_log(user_id, date, start_time, end_time, suspicious)
values(:user_id, :date, '7:00', '7:00', true)
on conflict (user_id, date)
do update set suspicious = true
""")

@app.route('/time_log/ping', methods=['GET'])
def add_time_log():
    now = datetime.datetime.now().replace(microsecond=0)
    ip = flask.request.headers.get('X-Forwarded-For', flask.request.remote_addr)

    if not current_user or not current_user.is_authenticated:
        return {"error": "Not logged in"}
    elif not ip.startswith("145.76.") and not ip.startswith("::ffff:145.76.") and ip != "127.0.0.1":
        return {"error": f"Not a Saxion IP ({flask.request.remote_addr})"}
    elif now.hour < 7 or now.hour >= 22:
        with db.engine.connect() as dbc:
            dbc.execute(SUSPICIOUS_QUERY, user_id=current_user.id, date=now.date())
        print(f"Time log outside time window: user_id={current_user.id} now={now}")
        return {"error": f"Outside 7:00-22:00 time window"}
    else:
        with db.engine.connect() as dbc:
            dbc.execute(TIME_LOG_QUERY, user_id=current_user.id, date=now.date(), time=now.time())
        return {"success": True}
