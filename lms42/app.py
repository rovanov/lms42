from flask import Flask, session, request, has_request_context
from flask_login import LoginManager 
from flask_sqlalchemy import SQLAlchemy
import flask_migrate
# import flask_monitoringdashboard as dashboard
import sqlalchemy
import os
import postgresqlite
from flask_wtf.csrf import CSRFProtect
import json

# Create and configure the app
app = Flask(__name__)
app.secret_key = os.environ.get('SESSION_KEY', 'Yw<@K8)^Q-&Dw')
app.config['DATA_DIR'] = os.environ.get("DATA_DIR", "data")
app.config['SQLALCHEMY_DATABASE_URI'] = postgresqlite.get_uri(f"{app.config['DATA_DIR']}/postgresqlite")
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
app.config['SQLALCHEMY_ENGINE_OPTIONS'] = {"isolation_level": "SERIALIZABLE"}
app.config['SESSION_COOKIE_SAMESITE'] = "Lax"
app.config['SEND_FILE_MAX_AGE_DEFAULT'] = 0
app.config['WTF_CSRF_TIME_LIMIT'] = None
app.config['TEMPLATES_AUTO_RELOAD'] = True
app.config['SESSION_COOKIE_DOMAIN'] = False
app.jinja_env.trim_blocks = True
app.jinja_env.lstrip_blocks = True

# Guard against CSRF on all PUT/POST requests, not only those using WTForms
csrf = CSRFProtect(app)

# Set up the Flask Monitoring Dashboard
#dashboard.config.username = "admin"
#dashboard.config.password = "changeme42"
#dashboard.config.security_token = os.environ['SESSION_KEY']
#dashboard.config.database = "sql"
#dashboard.config.database_name = 'sqlite:///data/flask_monitoringdashboard.db' 
#csrf.exempt(dashboard.blueprint)
#dashboard.bind(app)

# Disable static file caching in development mode
app.config['SEND_FILE_MAX_AGE_DEFAULT'] = 0 if os.environ.get('FLASK_ENV', 'development') == "development" else 3*3600

# Make session cookies last for 31 days
@app.before_request
def make_session_permanent():
    session.permanent = True

# Set the base_url based on the first request
base_url = None

@app.before_first_request
def set_base_url():
    global base_url
    protocol = request.headers.get('X-Forwarded-Proto') or request.headers.get('X-Forwarded-Protocol') or ('https' if request.headers.get('X-Forwarded-Ssl') == 'on' else 'http')
    base_url = protocol + "://" + request.host
    print(f"base_url set to {base_url}")
    app.config['SERVER_NAME'] = request.host 
    app.config['PREFERRED_URL_SCHEME'] = protocol

def get_base_url():
    return base_url

# Create a db instance
db = SQLAlchemy(app, metadata=sqlalchemy.MetaData(naming_convention=sqlalchemy.sql.schema.DEFAULT_NAMING_CONVENTION))

# Make sure indexes and constrains are given an explicit name.
db.Model.metadata = sqlalchemy.MetaData(
    naming_convention={
        "ix": "ix_%(column_0_label)s",
        "uq": "uq_%(table_name)s_%(column_0_name)s",
        "ck": "ck_%(table_name)s_%(constraint_name)s",
        "fk": "fk_%(table_name)s_%(column_0_name)s_%(referred_table_name)s",
        "pk": "pk_%(table_name)s"
    }
)

# Define the migrator, used by 'flask db upgrade'
migrate = flask_migrate.Migrate(app, db)

login_manager = LoginManager()
login_manager.login_view = '/user/login'
login_manager.init_app(app)

# Let the login manager know how to load user objects
from .models.user import User

@login_manager.user_loader
def load_user(user_id):
    return User.query.get(int(user_id))

from . import utils
app.jinja_env.filters['markdown'] = utils.markdown_to_html
app.jinja_env.filters['localtime'] = utils.utc_to_display
app.jinja_env.filters['date'] = utils.format_date
app.jinja_env.filters['short_date'] = utils.format_short_date
app.jinja_env.filters['json'] = json.dumps
app.jinja_env.globals['has_request_context'] = has_request_context

app.template_filter('urlencode')(utils.url_encode)


# Import all routes and models
from .routes import *
from .models import *
