"""Provides forms for the internship system.

There are two forms - one for a Company and one for a Team.
They match with the Company and Team database models.
"""

import flask_wtf
import wtforms as wtf
from flask_wtf.file import FileAllowed, FileField
from wtforms import validators as wtv
from wtforms.fields.html5 import IntegerField

class AddTeamForm(flask_wtf.FlaskForm):
    """Provides a form for editing or adding a team to the internship system.

    For adding a team into the database, make sure that they 'belong to' a
    Company database object.
    """

    name = wtf.TextField('Team name', validators=[wtv.InputRequired()])
    size = wtf.SelectField(
        'Team size',
        validators=[wtv.InputRequired()],
        choices=[
            ('1-4', '1 to 4 employees'),
            ('5-9', '5 to 9 employees'),
            ('10-14', '10 to 14 employees'),
            ('15+', '15 employees or more'),
        ])
    location = wtf.SelectField(
        'Where does the team work?',
        validators=[wtv.InputRequired()],
        choices=[
            ('Works at the office', 'In the office'),
            ('Works both in office and at home', 'Partly from home, partly in office'),
            ('Works from home', 'From home'),
        ])
    technologies = wtf.TextField(
        'Technologies/languages used',
        validators=[wtv.InputRequired()],
        )
    working_on = wtf.TextAreaField('What kind of projects does the team work on?')
    misc = wtf.TextField('Any other info the student should know?')
    contact = wtf.TextField(
        'How should the student proceed to apply?',
        validators=[wtv.InputRequired()],
        )
    submit = wtf.SubmitField('Submit')


class EditTeamForm(AddTeamForm):
    availability = wtf.SelectField('Open to interns?', choices=[
        (0, 'Yes!'),
        (1, 'Maybe or unclear'),
        (2, 'Not at this time'),
    ])


class EditCompanyForm(flask_wtf.FlaskForm):
    """Provides a form for editing/adding a company to the internship system."""

    name = wtf.TextField('Company name', validators=[wtv.InputRequired()])
    logo = FileField(
        'Logo',
        validators=[FileAllowed(['png'], 'PNG files only.')],
        )
    size = wtf.SelectField('Company size', choices=[
        ('1-9', '1 to 9 employees'),
        ('10-49', '10 to 49 employees'),
        ('50-249', '50 to 249 employees'),
        ('250+', '250 employees or more'),
        ])
    location = wtf.TextField('City')
    description = wtf.TextField('One-sentence description', validators=[
        wtv.InputRequired(),
        wtv.length(min=5, max=100),
        ])
    promotext = wtf.TextAreaField("Why it's great to work here")
    intern_fee = IntegerField(
        'Internship fee per month, based on 4 days a week, gross amount',
        validators=[wtv.InputRequired()],
        )
    misc = wtf.TextAreaField('Other info')

    submit = wtf.SubmitField('Save')
