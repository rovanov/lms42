from ..app import app, db
import sqlalchemy as sa
import datetime
from collections import deque
from backports.cached_property import cached_property
from .. import working_days

LAST_DAY_COUNT = 10


class Performance:

    def __init__(self, user):

        self.user = user
        self.time_logs = TimeLog.query.filter_by(user_id=user.id).order_by(TimeLog.date)
        self.absent_reasons = {ad.date: ad.reason for ad in AbsentDay.query.filter_by(user_id=user.id)}

        attempts_sql = """
        select max(a.avg_days) avg_days, date(min(a.submit_time))::date as submit_date
        from attempt a
        join "user" u on a.student_id=u.id
        where a.status='passed' and a.student_id = :student_id
        group by a.node_id
        order by min(a.submit_time)
        """
        with db.engine.connect() as dbc:
             self.attempts = dbc.execute(sa.sql.text(attempts_sql), student_id=user.id)


    @cached_property
    def last_days(self):
        last_time_logs = self.time_logs[-LAST_DAY_COUNT-1:]
        today = datetime.date.today()
        day = working_days.offset(today, -LAST_DAY_COUNT)
        results = []
        while day <= today:
            last_day = {
                "date": day,
                "reason": self.get_day_reason(day),
            }
            time_log = next(iter([time_log for time_log in last_time_logs if time_log.date == day]), None)
            if time_log:
                last_day.update({
                    "start": time_log.start_time.strftime("%H:%M"),
                    "end": time_log.end_time.strftime("%H:%M"),
                    "hours": time_log.hours,
                    "suspicious": time_log.suspicious,
                })
            results.append(last_day)
            day = working_days.offset(day, +1)

        return results


    def get_day_reason(self, date):
        return self.absent_reasons.get(date, self.user.default_schedule[date.weekday()])


    @cached_property
    def quarters(self):        
        attempts = deque(self.attempts)
        time_logs = deque(self.time_logs)

        last_year_quarter = working_days.get_current_quarter()
        if time_logs:
            year_quarter = working_days.get_current_quarter(time_logs[0].date)
        else:
            year_quarter = last_year_quarter

        results = []

        while year_quarter <= last_year_quarter:
            quarter_start, quarter_end = working_days.get_quarter_dates(*year_quarter, q4="extra_week")

            quarter = {
                "year": year_quarter[0],
                "quarter": year_quarter[1],
                "first": quarter_start,
                "last": quarter_end - datetime.timedelta(days=1),
                "hours": 0,
                "progress": 0,
                "nominal_days": 0,
                "nominal_progress": 0,
            }

            while attempts and attempts[0]["submit_date"] < quarter_end:
                attempt = attempts.popleft()
                quarter["progress"] += attempt["avg_days"]

            while time_logs and time_logs[0].date < quarter_end:
                time_log = time_logs.popleft()
                quarter["hours"] += time_log.hours

            today = datetime.date.today()
            for day in working_days.date_range(quarter_start, quarter_end):
                if day <= today and working_days.is_working_day(day):
                    reason = self.get_day_reason(day)
                    if reason != "absent_structural":
                        quarter["nominal_progress"] += 1
                    if reason not in ("absent_structural", "home_structural"):
                        quarter["nominal_days"] += 1

            results.append(quarter)

            if year_quarter[1] < 4:
                year_quarter = (year_quarter[0], year_quarter[1]+1)
            else:
                year_quarter = (year_quarter[0]+1, 1)

        return results



from .user import TimeLog, AbsentDay
