from ..app import db, app
import datetime
import sqlalchemy
import json
import enum
import os
import flask
from .grading import Grading
from .. import utils, working_days, email
from .user import User


FORMATIVE_ACTIONS = {
    "passed": "The student passes.",
    "repair": "The student needs to correct some work.",
    "failed": "The student needs to start anew.",
}


class Attempt(db.Model):
    id = db.Column(db.Integer, primary_key=True)

    __table_args__ = (
        db.Index('attempt_status_index', 'status', 'student_id'),
        db.UniqueConstraint('student_id', 'node_id', 'number'),
    )

    student_id = db.Column(db.Integer, db.ForeignKey('user.id'), nullable=False)
    student = db.relationship("User", foreign_keys="Attempt.student_id", back_populates="attempts")

    number = db.Column(db.Integer, nullable=False)

    node_id = db.Column(db.String, nullable=False)
    variant_id = db.Column(db.SmallInteger, nullable=False)

    start_time = db.Column(db.DateTime, default=datetime.datetime.utcnow, nullable=False)
    deadline_time = db.Column(db.DateTime)
    upload_time = db.Column(db.DateTime)
    submit_time = db.Column(db.DateTime)

    status = db.Column(db.String, nullable=False) # awaiting_approval, in_progress, needs_grading, needs_consent, passed, repair, failed
    finished = db.Column(db.String) # yes, deadline, forfeit, accidental

    credits = db.Column(db.Integer, nullable=False, default=0)
    
    mentor_id = db.Column(db.Integer, db.ForeignKey('user.id'))
    mentor = db.relationship("User", foreign_keys="Attempt.mentor_id")

    avg_days = db.Column(db.Float, nullable=False)

    alternatives = db.Column(db.JSON, default=dict(), nullable=False)

    @property
    def latest_grading(self):
        return self.gradings.order_by(Grading.id.desc()).first()

    @property
    def directory(self):
        return os.path.join(app.config['DATA_DIR'], 'attempts', f"{self.node_id}-{self.student_id}-{self.number}")

    @property
    def node_name(self):
        node = curriculum.get('nodes_by_id')[self.node_id]
        name = node["name"]
        topic = curriculum.get('modules_by_id')[node["module_id"]]
        if topic and topic.get("name"):
            name = topic.get("short", topic.get("name")) + " ➤ " + name
        return name

    @property
    def hours(self):
        return working_days.get_working_hours_delta(self.start_time, self.submit_time or datetime.datetime.utcnow())

    def write_json(self):
        d = utils.model_to_dict(self)
        d["student"] = {
            "full_name": self.student.full_name,
            "email": self.student.email,
        }
        d["gradings"] = [utils.model_to_dict(grading) for grading in self.gradings]
        with open(os.path.join(self.directory, "attempt.json"), "w") as file:
            file.write(json.dumps(d,indent=4))


    # def assign_mentor(self, exclude_mentor_id=-1):
    #     self.mentor_id = None

    #     if self.credits > 0:
    #         return

    #     # Determine the day of the week that the work will (mostly) take place, 
    #     # so that we can filter out students that are absent on that day.
    #     now = datetime.datetime.now()
    #     date = working_days.offset(now.date(), 1 if now.hour >= 15 else 0)
                
    #     result = db.session.execute("""
    #         select id, badness
    #         from (
    #             select u.id, (
    #                 coalesce(max(case when
    #                     a.status='passed' then 0
    #                     when a.status='needs_grading' then 20
    #                     else 10
    #                 end), 0) +
    #                 (
    #                     select coalesce(sum(case
    #                         when t.status='in_progress' then 100000
    #                         when t.student_id = :student_id then 3
    #                         else 1
    #                     end), 0)
    #                     from attempt t
    #                     where t.mentor_id=u.id
    #                 )
    #             ) as badness
    #             from attempt a 
    #             join "user" u on u.id = a.student_id
    #             where
    #                 a.node_id = :node_id
    #                 and a.variant_id = :variant_id
    #                 and (
    #                     a.status='passed'
    #                     or a.status='needs_grading'
    #                     or (a.status='in_progress' and a.mentor_id is null)
    #                 )
    #                 and a.student_id != :student_id
    #                 and u.id != :exclude_mentor_id
    #                 and u.level = 10
    #                 and u.class_name = :class_name
    #                 and u.is_active = True
    #                 and u.is_hidden = False
    #                 and not (:weekday = any (u.absent_days))
    #                 and not exists(select 1 from attempt t where t.id=u.current_attempt_id and t.credits>0)
    #                 and not exists(select 1 from absent_day ad where ad.user_id=u.id and ad.date=:date)
    #             group by u.id
    #         ) as user_badness
    #         where badness < 300000
    #         order by badness asc
    #         limit 1
    #     """, {
    #         "student_id": self.student_id,
    #         "node_id": self.node_id,
    #         "variant_id": self.variant_id,
    #         "weekday": date.weekday(),
    #         "date": date,
    #         "class_name": self.student.class_name,
    #         "exclude_mentor_id": exclude_mentor_id,
    #     })

    #     self.mentor_id = result.scalars().first()

        # These emails have been disabled as we've been receiving spam reports.
        # if self.mentor_id:
        #     mentor = User.query.get(self.mentor_id)

        #     email.send(mentor.email, "New mentee: "+self.student.first_name, f"Hi {mentor.first_name},\n\n{self.student.full_name} is your new mentee.\nThanks for helping!")
        #     if exclude_mentor_id > 0: # We're reassigning
        #         email.send(self.student.email, "Mentor changed: "+mentor.first_name, f"Hi {self.student.first_name},\n\nYour previous mentor is unfortunately no longer available.\nHowever... we found you a fresh one! {mentor.full_name}\nDon't be shy asking for help!")
        # elif exclude_mentor_id > 0: # We're reassigning
        #     email.send(self.student.email, "Mentor unavailable", f"Hi {self.student.first_name},\n\nYour previous mentor is unfortunately no longer available,\nand we were unable to find you a new one.\nNeed help? Ask a smart-looking teacher! (Where available.)")


def get_notifications(attempt, grading):
    notifications = []

    if attempt.status == "in_progress":
        notifications.append("In progress!")

    if attempt.submit_time:
        notifications.append(f"Attempt by {attempt.student.full_name} from {utils.utc_to_display(attempt.start_time)} until {utils.utc_to_display(attempt.submit_time)}.")
    else:
        notifications.append(f"Attempt by {attempt.student.full_name} started {utils.utc_to_display(attempt.start_time)}.")

    if attempt.deadline_time:
        if attempt.status == "in_progress":
            notifications.append(f"Deadline: {utils.utc_to_display(attempt.deadline_time)}.")

        exceed = working_days.get_working_hours_delta(attempt.deadline_time, attempt.submit_time or datetime.datetime.utcnow())
        if exceed > 0:
            notifications.append(f"<strong>Deadline exceeded</strong> by {round(exceed,1)} hours!")

    if attempt.finished in ["forfeit", "deadline", "accidental"]:
        notifications.append(f"Attempt not finished: <strong>{attempt.finished}</strong>.")

    if grading:
        notifications.append(f"Graded by {grading.grader.full_name} on {utils.utc_to_display(grading.time)}.")
        if grading.needs_consent:
            notifications.append(f"Awaiting consent by a second examinator.")
        else:
            if grading.consent_user_id:
                notifications.append(f"Consent given by {grading.consenter.full_name} on {utils.utc_to_display(grading.consent_time)}.")
            if grading.grade_motivation:
                notifications.append(f"Teacher feedback:<div class='notification is-info'>{utils.markdown_to_html(grading.grade_motivation)}</div>")
            if attempt.status in FORMATIVE_ACTIONS:
                notifications.append(f"<strong>{FORMATIVE_ACTIONS[attempt.status]}</strong>")

    notifications.append(f"Mentor: {attempt.mentor.full_name if attempt.mentor_id else 'none'}.")

    return notifications



from . import curriculum
