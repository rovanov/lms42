from smtplib import SMTP
import datetime
import os
import flask

def send(to_addr, subject, message_text):
    host = os.environ.get('SMTP_HOST','').strip()
    user = os.environ.get('SMTP_USER','').strip()
    password = os.environ.get('SMTP_PASSWORD','').strip()
    from_addr = f"Saxion Ad SD <{user}>"

    headers = {
        'Content-Type': 'text/plain; charset=utf-8',
        'Content-Disposition': 'inline',
        'Content-Transfer-Encoding': '8bit',
        'From': from_addr,
        'To': to_addr,
        'Date': datetime.datetime.now().strftime('%a, %d %b %Y  %H:%M:%S %Z'),
        'X-Mailer': 'python',
        'Subject': subject
    }

    msg = ''
    for key, value in headers.items():
        msg += f"{key}: {value}\n"
    msg += f"\n{message_text}\n"

    if not host:
        with open('/tmp/lms42-email.txt', 'w') as file:
            file.write(msg)
        flask.flash("Please configure an SMTP server in config.sh. Email written to /tmp/lms42-email.txt")
        return

    smtp = SMTP(host=host, port=587)
    smtp.connect(host, 587)
    smtp.starttls()
    if user:
        smtp.login(user, password)

    smtp.sendmail(from_addr, to_addr, msg.encode("utf8"))
    smtp.quit()
