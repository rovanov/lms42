## Curriculum file format documentation

This is work-in-progress is still very much incomplete. Help is more than welcome!

### Directory structure

TODO.

### index.yaml

Contains meta information about a module. TODO.

An incomplete list of properties:
- `name`: TODO.
- `short`: TODO.
- `description`: TODO.
- `index`: TODO.
- `owner`: TODO.

### node.yaml

Contains meta information for a node (a lesson or an exam). It may also include the assignment(s), unless the node is an exam.

An incomplete list of properties:

- `name`: TODO.
- `description`: TODO.
- `days`: TODO. Defaults to 1.
- `ects`: Number of credits a student receives on successful completion of this lesson. Settings this marks the lesson as an exam (project). 
- `miller`: TODO.
- `goals`: TODO.
- `plans`: TODO.
- `resources`: TODO.
- `intro`: TODO.
- `TODO`: TODO.
- `public`: TODO.
- `type`: TODO.
- `start_when_ready`: TODO.
- `avg_attempts`: TODO. When not set, calculated as 1.5 * days for regular nodes and 1.0 * days for exams.
- `allow_longer`: TODO.
- `ignore`: TODO.
- `grading`: TODO.
- `upload`: TODO.
- `feedback`: When `False`, don't show the feedback form. Defaults to `True`.

### assignment?.yaml

TODO.

#### Objectives

TODO.

An incomplete list of properties:

- `title`: TODO.
- `text`: TODO.
- `0`, `1`, `2`, `3`, `4`: The rubric ratings. At least one of these (or `scale`) needs to be specified. (Or, as happens frequently, included from `reference.yaml` using a `^merge`.)
- `weight`: TODO. Not allowed for exams - use map instead.
- `map`: TODO. Required for exam objectives.
- `code`: TODO.
- `bonus`, `malus`: TODO.
- `scale`: When set to `10`, grading happens with a number 1..10, instead of on a 5-point scale.

#### Alternatives

`title` and `text` fields in objectives can contain *alternatives*, causing students to randomly get (small) variations on an assignment. This can be especially useful for exams. They can be used like this:

```yaml
-
    title: Implement [widget:text]{a Text}[widget:image]{an Image} widget
    text: |
        Use your awesome powers to implement a widget that allow the user to view [widget:text]{many lines of text}[widget:image]{a png or jpg image}.
    4: This is a rubric.
```

