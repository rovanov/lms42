## LMS42

This project contains the Learning Management System as well as (most of) the curriculum for the Associate degree Software Development at Saxion University of Applied Sciences. We highly encourage students to submit merge requests to improve upon either.

### Contributing guidelines

Are you thinking about contributing to LMS42 in any way? Great! Please have a look at the [Contributing Guide](CONTRIBUTING.md).

### Running

To run this project, you'll need:
- Linux (though OSX seems to mostly work)
- Python 3.6+
- Poetry
- libpq-dev

On Arch/Manjaro Linux you can install these using `sudo pacman -Sy docker python postgresql-libs python-poetry`.

Running `bin/sd42 dev` should be enough to get a development server up and running on `http://localhost:8000`. This will also create a `config.sh` script that you may want to customize.

### Database

The default settings will cause `bin/sd42 dev` to download and start (and later on auto-stop) a PostgreSQL database working within the local `data/` directory. This currently only works on x64 Linux. In case you want to use some external PostreSQL server (which is recommended for production purposes or when running on a non-supported platform), you should customize `data/postgresqlite/postgresqlite.json`. Read the [PostgreSQLite documentation](https://github.com/vanviegen/postgresqlite#the-config-file) for more info.

### Users

When the database is first created, there won't be any users in it. The first email address you enter at `http://localhost:8000/user/login` will become the administrator user. A login link will be emailed (see the next section).

For development, having to login through confirmation emails all the time can be cumbersome. By setting `ALLOW_TILDE_LOGIN` to `yes` in `config.sh`, users will be able to login immediately (and without any authentication) by prefixing their email addresses with a ~tilde. For instance: `~test@example.com`. Obviously, this should *not* be used in production.

### Mail server

An SMTP server and credentials should be configured in `config.sh` for outgoings emails. Until that is done, outgoing emails will be written to `/tmp/lms42-email.txt` - which is probably fine for most development work.

### Deploying to production

`bin/sd42 deploy` will attempt to build the curriculum and the SASS styling, and [rsync](https://en.wikipedia.org/wiki/Rsync) everything except data, config and caches to the `DEPLOY_TARGET` configured in `config.sh`.

The production server can be started using `bin/sd42 prod`. It is recommended to do this from a [systemd](https://en.wikipedia.org/wiki/Systemd) service (or similar), such that the application automatically attempts to restart in case of a problem. Besides running the Flask application (with auto reload enabled), the `bin/sd42 prod` script will automatically `npm install` when `package.json` changes, `poetry install` when `pyproject.toml` changes, and do a database upgrade when files are added to `migrations/versions`.

It is highly recommended to use a reverse proxy server (such as Nginx) to provide `https` and to redirect all `http` requests to `https`.

### Restricted files

For obvious reasons, exams and 'official' solutions to the assignments should not be viewable by the general public. They *are* part of this repository though, albeit encrypted using `git-crypt`. When working on this repository without setting the appropriate shared key, files named `curriculum/**/-exam/**` and `curriculum/**/solution[0-9]/**` will contain encrypted binary gibberish.

Those who need access to the restricted files (presumably teachers) should ask for a copy of the shared key file. It can be used by installing `git-crypt` (probably provided by your distribution) and running `git crypt unlock <keyfile>` in the repository. You'll need to do this only once and the key file can be deleted after that. Restricted files (as determined by the `.gitattributes` file) will show up unencrypted in your working directory. They will be encrypted/decrypted on-the-fly when commiting, checking out, diffing, etc. More info on `git-crypt` can be found [here](https://betterprogramming.pub/safely-keep-your-application-secrets-in-repos-even-if-theyre-public-using-git-crypt-735d74b011bd). In order to perform merges on encrypted files, you will need to run this command in your repository once: `git config --local include.path ../.gitconfig`.

### Notable files and directories

| Path              | Description |
| -----             | --------- |
| config.sh         | Local settings. Restart your server after modifying. |
| bin/sd42          | The start script. Use `bin/sd42 help` to view supported commands. |
| curriculum/       | Directory structure describing all lesson content. |
| lms42/            | All Python source code. |
| lms42/templates/  | Contains jinja2 html templates. |
| lms42/static/     | Static web files served as /static/*. |
| css/style.scss    | Main stylesheet, compiled to lms42/static/style.css. |
| migrations/       | Database migrations, managed by Alembic. |


