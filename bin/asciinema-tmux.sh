#!/bin/sh

PWD=`pwd`
BASEDIR=$(cd `dirname ${0}`; pwd)
tmux set-option -g prefix C-s
asciinema rec -i 3 -c "tmux -L asciinema-tmux set-option -g default-command '/bin/bash --noprofile --rcfile $BASEDIR/asciinema-tmux.bashrc' \; new-session -A -c '$PWD' \; set-option -g status off"
tmux set-option -g prefix C-a

