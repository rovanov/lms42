#!/bin/env python3

from glob import glob
import subprocess
import pickle
import html

results = {}

for dir in glob('curriculum/*/*/*-exam') + glob('curriculum/*/*/*-project') + glob('curriculum/*/*/*-graduation'):

    variants = {}
    for file in glob(f'{dir}/assignment*.yaml') + [f'{dir}/node.yaml']:
        cmd = ["git", "log", "--follow", "--patch", "--word-diff=porcelain", "--pretty=format:patch %cI %s", "--", file]
        proc = subprocess.run(cmd, capture_output=True, encoding='utf-8')
        patches = []
        for patch in proc.stdout[6:].rstrip("\n").split("\npatch "):
            lines = patch.split("\n")
            date, descr = lines[0].split(' ', 1)
            date = date[:10]

            if descr in ['Git crypt restricted', 'Squashed all commits to remove exams from history', 'Programme periods can now have multiple groups. Happier, healthier, more effective.']:
                continue

            changes = []
            meta = []
            for line in lines[1:]:
                if line[0:3]=='@@ ':
                    changes.append('')
                elif line=='' or line.split(' ',1)[0] in ['index', '+++', '---', 'diff', 'similarity']:
                    pass
                elif line[0] == ' ':
                    changes[-1] += html.escape(line[1:])
                elif line[0] == '+':
                    changes[-1] += f"<span class='new'>{html.escape(line[1:])}</span>"
                elif line[0] == '-':
                    changes[-1] += f"<span class='old'>{html.escape(line[1:])}</span>"
                elif line[0] == '~':
                    changes[-1] += "\n"
                else:
                    meta.append(line)
                    
            patches.append({
                "date": date,
                "description": descr,
                "changes": changes,
                "meta": meta,
            })
    
        basename = file.split('/')[-1]
        if basename=="node.yaml":
            variant_name = "Common"
        else:
            variant_name = f"Variant {basename[10]}"
        variants[variant_name] = patches

    module_id = dir.split('/')[2]
    results[module_id] = variants
        
    

with open("exam-changes.pickle", "wb") as file:
    file.write(pickle.dumps(results))
