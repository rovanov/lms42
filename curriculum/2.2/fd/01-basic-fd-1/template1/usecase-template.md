# <nr> <name>
## Goal
The goal of the use case
## Pre
(pre condition)
What we expect is already the state of the world
## Steps
(description)
1. <the first step of the scenario> 
2. <consecutive steps of the scenario until the goal has been reached>
..
## Alternative paths
(extensions)
1.a <the deviation of step 1 described above>
	1. <the first step of the alternative scenario> 
    2. <optional consecutive steps of the alternative scenario>
    ..
## Post
(post condition/success end condition) 
The state of the world upon successful completion