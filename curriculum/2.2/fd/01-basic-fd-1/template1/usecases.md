# 1. Register pet
## Goal
Register your pet for dating
## Pre
## Steps
1. Fill in the pet type and gender  
2. Provide the character traits of your pet
3. Describe the preferences your pet searches for a date.
## Post
You pet has completed the profile on the website.

