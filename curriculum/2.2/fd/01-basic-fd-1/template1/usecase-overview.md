# Use case overview

```plantuml
left to right direction
actor "User" as user
rectangle DatingApp {
  usecase "Register pet" as UC1
  usecase "..." as UC2
}
user --> UC1
user --> UC2
```