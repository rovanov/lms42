name: Complex functional design 1
description: Create a functional design from scratch for a complex case.
goals:
    requirements: 1
    interview: 1
    wireframing: 1
    datamodeling: 1
    usecases: 1
days: 2
resources:
    -
        link: https://www.markdownguide.org/basic-syntax
        title: Basic Markdown Syntax
        info: Nearly all Markdown applications support the basic syntax outlined in this document. There are minor variations between Markdown viewers.
assignment:
    - Introduction: |
        Just like in the previous assignment you are asked to create a functional design for a complex use case. This time you are going to have to do it on your own.

    - Assignment: |
        Casus: ticket management met dependencies en milestones
        Case dating website for animals

    - Requirements:
      - 
        link: https://www.youtube.com/watch?v=D6R7njeK-qg&t=290s
        title: Functional requirements
        info: Watch until 5:30
      - 
        link: https://www.youtube.com/watch?v=He2lnh_YSFA
        title: Quick & Simple Explanation of Non-Functional Requirements and Why They matter?
        info: Non functionals
      -
        link: https://www.youtube.com/watch?v=v1-b2hspIb4
        title: Functional vs non functionals
        info: Functional vs non functionals
      -
        text: |
          Write ate least 10 clear and concise functional requirements
          Write ate least 3 clear and concise non-functional requirements
          
        map:
          requirements: 1
        0: No questions
        4: Relevant question including well thought out answers.


    - Interview question:
      -
        link: https://www.youtube.com/watch?v=V_9LaiJoJ3g
        title: "Business Analyst Day in the Life : Requirements Gathering Techniques & How Ask the Right Questions"
        info: Video explaining the importance understanding the context and ask the right questions.
      - 
        text: |
          Watch the entire video and try to understand what the customer wants to have build. After you have a clear picture of what needs to be build watch the video again and try to think of issues that still are unclear. What are things you still need to know before you can start writing your code. 

          Create a list of at least 5 questions you would like to ask the customer to clarify some of the features. Be specific in your questions. Make sure your questions are clear and relevant.
        map:
          interview: 1
        0: No questions
        4: Relevant question including well thought out answers.

    - Wireframes:
      - 
        link: https://www.justinmind.com/blog/low-fidelity-vs-high-fidelity-wireframing-is-paper-dead/
        title: "Low fidelity vs high fidelity wireframes: what's the difference?"
        info: A blog explaining the difference between low - and high fidelity wireframes and what the benefits of each is.
      -
        text: |
          With wireframes you can create a visual representation of what your application will look like. For this objective you are asked to create low fidelity wireframes. Your design should show which visual elements are found in your application screens and how the user can interact with these elements.

          You can create your wireframes with pen and paper (recommended) or you can use a digital tool like [www.draw.io](https://www.draw.io). If you use draw.io be sure to select the Mockup symbols (click on "+ More Shapes..." and select "Mockups").
          
        0: Wireframes do not give a clear impression of the application to be build
        4: Clear wire frames including intuitive navigation between pages.
        map:
          wireframing: 1
        
    - Usecases:
      - 
        link: https://www.youtube.com/watch?v=uN-zt7bXbnQ
        title: Design a Use Case Diagram 
        info: A basic introduction to the UML notation for modeling use cases
      - 
        link: http://cis.bentley.edu/lwaguespack/CS360_Site/Downloads_files/Use%20Case%20Template%20(Cockburn).pdf
        title: Basic Use Case Template
        info: Our use case template which can be found in the template folder is based on the template developed by use case guru Alistair Cockburn (pronounce co-burn). 
      - 
        link: https://plantuml.com/use-case-diagram
        title: PlantUML - Use Case Diagram
        info: Describes how to create Use Case Diagrams using PlantUML.
      - 
        text: |
          Write at least 5 use case diagrams for the provided use case. Create an overview of all the use case diagrams and for at least 5 use case write out the goals, pre-condition, steps, post-conditions as described in the *usecase-template.md* file. Also write the alternative paths for possible error cases.

        0: Use cases do not give a clear impression of the application to be build
        4: Complete overview of all use cases including detailed description of each use case.
        map:
          usecases: 1
        
    - Domain model:
      -
        link: https://www.youtube.com/watch?v=jtWj-rreoxs
        title: Map out a Domain Model
        info: Introduction into how map entities from use cases to a domain model.
      -
        text: |
          Create a domain model for the application you are going to build. You domain model should look similar to a logical data model in which the entities are described including the relationship between them.

        0: Domain model does not give a clear impression of the application to be build
        4: Domain contains all entities with proper relationship (including correct cardinality).
        map:
          datamodeling: 1
