## Objective #1: Advice that appeals to you

(Describe some of the advice that appeals to you personally.)


## Objective #2: Procrastination

(How big of a problem is procrastination for you?)

(What are you weak spots?)

(Are you already employing some strategies against procrastination?)

(Describe some procrastination reduction strategies that appeal to you.)


## Objective #3: Things you'll try

(Which of the general and/or procrastination advice above will you actively try to apply over the next two weeks? In what way will you work each of them into your routine, so you won't forget about them?)
