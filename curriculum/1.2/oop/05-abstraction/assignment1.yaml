- |
    We'll be extending and refactoring Tic Tac Toe in an object-oriented way.

    A high-level class diagram for what should be the resulting application by the end of all objectives:

    ```plantuml
    Game o--> "2" Player
    Game o--> Grid
    class Player {
        +get_mark(): str
        +get_move(): (int,int)
    }
    abstract class Grid {
        +__init__(width, height)
        +set(x, y, value)
        +get(x, y): str
        +{abstract}draw()
    }
    Grid <|-- SimpleGrid
    Grid <|-- FancyGrid
    
    Player *--> DecisionMaker

    abstract class DecisionMaker {
        +{abstract}get_move(board: Grid, mark: str): (int,int)
    }
    DecisionMaker <|-- HumanDecisionMaker
    DecisionMaker <|-- SequentialDecisionMaker
    DecisionMaker <|-- RandomDecisionMaker
    ```

-   Create a grid:
        ^merge: feature
        text: |
            In the provided "ui.py" file you will find a code snippet for a grid class. Implement the class so that you can create grids of different sizes. Note that you should provide a width and a height to the class and it should dynamically render the grid on screen as follows - for example for a 2 x 2 grid:
            ```
             -------
            | 0 | 1 |
             -------
            | 2 | 3 |
             -------
            ```
            Or a 1 x 3 grid:
            ```
             ---
            | 0 |
             ---
            | 1 |
             ---
            | 2 |
             ---
            ```
            Or a 4 x 2 grid:
            ```
             ---------------
            | 0 | 1 | 2 | 3 |
             ---------------
            | 4 | 5 | 6 | 7 |
             ---------------
            ```

-   Implement the grid in blessed:
    -
        link: https://blessed.readthedocs.io/en/latest/api.html
        title: Blessed documentation
        info: Blessed is a library for creating terminal based user-interfaces. It helps you to position the cursor, use colors, handle input, and many other things.

    -
        ^merge: feature
        weight: 2.0
        text: |
            Blessed is a library for making terminal apps. It provides an easy way to display text in color in a terminal window. Make an implementation of a FancyGrid class which renders a grid using blessed. A 4 x 2 grid should look something like this:
            
            ![4x2 color grid](grid.png)

            You will want to install the `blessed` library using Poetry. Here are some examples on how to use it:

            ```python
            term = Terminal()

            # Move to location (0,0) -> term.home, clear the terminal window and move 5 lines down.
            print(term.home + term.clear + term.move_y(3))

            # Print a line of white spaces with some RGB background color (red in this case).
            print(term.move_x(1) + term.on_color_rgb(255, 0, 0) + '     ~     ')

            # Use predefined colors to set the foreground (black) and background (darkkhaki).
            print(term.move_x(2) + term.black_on_darkkhaki(' FancyGrid '))
            print(term.move_x(3) + term.on_blue('     ~     '))

            # Move the 'cursor' 10 lines down.
            print(term.move_y(10))
            ```

-   Create an abstract class for the Grid:
        ^merge: feature
        weight: 0.5
        text: |
            Create an *abstract* `Grid` class that provides an *abstract method* for outputting the grid to the terminal. Have both of your grid implementation classes *implement* your abstract base class.

-   Refactor your Tic Tac Toe game:
        ^merge: feature
        weight: 2
        text: |
            During the Python programming module you have created an implementation for Tic Tac Toe (in the lists assignment). Copy that implementation into this project, and refactor it to make proper use of object orientation.

            Your new implementation should make use of your `Grid` implementations to draw the grid. Add an easy to use way to display `X` and `O` characters (in appropriate colors, in case of the fancy grid) instead of the number in these classes. Users should be able to select if they want to see a simple grid or a fancy grid on startup.
            
-   Implement an AI player:
    -
        ^merge: feature
        text: |
            Let's add optional AI players to your game. There should be two different AIs, employing different strategies:

            - The Sequential bot always goes from left to right, from top to bottom, always choosing the first available square it encounters.
            - The Random bot just chooses a random available square. (If you feel so inclined, you can instead create a bot that actually tries to win!)

            When starting the game, it should begin by asking for each of the two players who will be playing: a (h)uman, (s)equential or (r)andom. Based on this, instantiate a `HumanDecisionMaker`, `SequentialDecisionMaker` or a `RandomDecisionMaker` object.

            Whenever the game requires a player to choose a square, it just asks the decision maker object for that player to make a choice.

    -
        link: https://www.programiz.com/python-programming/user-defined-exception
        title: Python Custom Exceptions
        info: Sometimes you may need to create your own custom exceptions that serve your purpose. This tutorial shows you how.

    -   text: |
            While playing an exciting game of Tic Tac Toe, sometimes the situation is just hopeless. You just want to give up...

            Allow `DecisionMaker`s to forfeit the game by throwing a custom exception. That exception (and only that one) should be caught by the main game logic, and handle the player's forfeit. Add a way for the `HumanDecisionMaker` to forfeit (such as the player typing `f`) and add a condition under which one of the computer players forfeits.
        ^merge: feature
    