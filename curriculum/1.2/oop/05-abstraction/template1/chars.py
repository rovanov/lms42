# Lines
VLINE = "│"
HLINE = "─"
# Corners 
LL_CORNER = "└"
UL_CORNER = "┌"
LR_CORNER = "┘"
UR_CORNER = "┐"
# Tee's
T_TEE = "┬"
B_TEE = "┴"
L_TEE = "├"
R_TEE = "┤"
# Cross
CROSS = "┼"