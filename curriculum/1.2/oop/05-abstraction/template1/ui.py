from blessed import Terminal

# 
# Code snippet blessed
# The code snippet shows how blessed can be used to diplay text (in different colors) 
# on the screen and how to move the cursor around.
# 
term = Terminal()

# Move to location (0,0) -> term.home, clear the terminal window and move the cursor 5 lines down 
# and 10 columns to the right. We're using end='' to prevent a newline character from being added.
print(term.home + term.clear + term.move_y(5) + term.move_x(10), end='')

# Print a message, using just a regular print.
print("Hi mom!")

# Change the current background color to an RGB (red, green blue) value. In this case: purple.
print(term.on_color_rgb(128, 0, 128), end='')
print("I have a purple background.")

# Change the foreground and the background color, print some text, and then reset the colors
# to their defaults.
print(term.black_on_darkkhaki("I'm black on dark khaki."))

print("I'm just black on white.")


class Grid():

    def draw(self):
        """Displays the grid on the screen"""
        # TODO: Implement draw logic
        pass
