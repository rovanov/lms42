import pygame, sys
from random import randint

FPS = 60    

SCREEN_WIDTH = 1000
SCREEN_HEIGHT = 600
PLAYER_SIZE = 30
MIN_WALL_WIDTH = 15
MAX_WALL_WIDTH = 200
MIN_WALL_INTERVAL = 50
MAX_WALL_INTERVAL = 200

X_SPEED = 4
Y_SPEED_DELTA = 0.5

ORANGE = (245, 95, 0)
BLACK = (0, 0, 0)
BLUE = (0, 0, 255)
RED = (255, 0, 0)

# Set up pygame
pygame.init()
screen = pygame.display.set_mode((SCREEN_WIDTH, SCREEN_HEIGHT))
screen.fill((0,0,0))
pygame.display.set_caption("Game")
clock = pygame.time.Clock()

# Load and convert the background image only once
background_img = pygame.image.load("background.jpeg")
background_img.convert()

# Main loop
started = False
while True:
    # Draw background
    rect = background_img.get_rect()
    rect.center = SCREEN_WIDTH / 2, SCREEN_HEIGHT / 2 
    screen.blit(background_img, rect)
    pygame.draw.rect(screen, (0,0,0), rect, 1)

    # Removing the event loop below will not display the pygame window ¯\_(ツ)_/¯ 
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            pygame.quit()
            sys.exit(0)
    
    # Get pressed keys and pass them to the library and the game objects
    keys = pygame.key.get_pressed()

    if not started:
        if keys[pygame.K_SPACE]:
            started = True
            player_position = { 'x': SCREEN_WIDTH - 2*PLAYER_SIZE, 'y': PLAYER_SIZE }
            
            walls = []
            next_wall_ticks = 0 # create the first wall immediately
            y_speed = 0

        # Display instruction text
        font = pygame.font.SysFont(None, 48)
        text_img = font.render("Press 'spacebar' to start", True, RED)
        screen.blit(text_img, ((SCREEN_WIDTH / 2) - 160, SCREEN_HEIGHT / 2))
    else:
        # Create new walls every now and then
        next_wall_ticks -= 1
        if next_wall_ticks <= 0:
            width = randint(MIN_WALL_WIDTH, MAX_WALL_WIDTH)
            height = SCREEN_HEIGHT - PLAYER_SIZE*3
            y = randint(-height+100, SCREEN_HEIGHT-100)
            walls.append({'x': SCREEN_WIDTH, 'y': y, 'w': width, 'h': height})

            next_wall_ticks = (width + randint(MIN_WALL_INTERVAL, MAX_WALL_INTERVAL)) / X_SPEED

        # Move the walls
        for wall in walls:
            if wall['x'] + wall['w'] < 0: # This wall is no longer visible
                walls.remove(wall)
                continue

            wall['x'] -= X_SPEED

        # Move the player
        if keys[pygame.K_UP]:
            y_speed -= Y_SPEED_DELTA
        elif keys[pygame.K_DOWN]:
            y_speed += Y_SPEED_DELTA
        else:
            y_speed += Y_SPEED_DELTA / 2

        y_speed *= 0.98
        player_position['y'] += y_speed

        bounce = False
        if player_position['y'] + PLAYER_SIZE >= SCREEN_HEIGHT or player_position['y'] < 0:
            bounce = True

        # Collision detection
        for wall in walls:
            if player_position['x']+PLAYER_SIZE > wall['x'] and player_position['x'] < wall['x']+wall['w'] and \
                player_position['y']+PLAYER_SIZE > wall['y'] and player_position['y'] < wall['y']+wall['h']:
                if abs(player_position['x']+PLAYER_SIZE - wall['x']) <= X_SPEED:
                    # Hold the player back
                    player_position['x'] = wall['x'] - PLAYER_SIZE
                else:
                    # Bounce the player on the top/bottom of the wall
                    bounce = True

        if bounce:            
            y_speed = -y_speed
            player_position['y'] += y_speed


        if player_position['x'] + PLAYER_SIZE < 0:
            # Game over
            started = False

        # Draw the walls
        for wall in walls:
            rect = ((wall['x'], wall['y']), (wall['w'], wall['h']))
            pygame.draw.rect(screen, BLUE, rect)

        # Draw the player
        pygame.draw.circle(screen, ORANGE, (player_position['x']+PLAYER_SIZE/2, player_position['y']+PLAYER_SIZE/2), PLAYER_SIZE/2)

    # Actually output our image to the screen
    pygame.display.update()

    # Make sure we don't run at more than 30 FPS.
    clock.tick(FPS)
