import random

INITIAL_HEALTH = 100


def ask_choice(prompt, choices):
    """Asks the user to choose one of the provided choices.

    Parameters
    ----------
    prompt: str
        The question to ask the user.
    choices: list or set
        The options the user can choose from. Each choice can be a string, or any other type
        as long as it can be converted to a string (using an `__str__` method).

    Returns
    -------
    The selected item out of choices.
    """

    choices = list(choices) # in case choices is a set, we want to convert it to a list
    while True:
        print(prompt)
        if not len(choices):
            print("  No options.")
            return
        for index, choice in enumerate(choices):
            print(f"  {index+1}. {str(choice).capitalize()}")
        index = ask_int("> ") - 1
        if 0 <= index < len(choices):
            return choices[index]

def ask_int(prompt="", min_value=None, max_value=None):
    """Like `input()`, but keeps repeating the question until the user enters a non-negative integer,
    that lies between the given boundaries.

    Parameters
    ----------
    prompt: str
        The question to ask the user.
    min_value: int or None
        An optional minimum allowed value (inclusive).
    max_value: int or None
        An optional maximum allowed value (inclusive).

    Returns
    -------
    int
        The value entered by the user.
    """
    while True:
        result = input(prompt)
        if result.isdigit():
            result = int(result)
            if (min_value==None or result>=min_value) and (max_value==None or result<=max_value):
                return result


class Game:
    def __init__(self):
        """The constructor. It asks the user how many players should participate,
        creates objects for each, and stores them in a list in the `players`
        attribute."""
        count = ask_int("How many players? ", 2, 6)
        self.players = [Player() for num in range(count)]

    def run(self):
        """Runs the actual game until someone wins."""
        turn = 0
        while not self.declare_winner():
            player = self.players[turn % len(self.players)]
            if not player.is_dead() > 0: # Skip dead players
                self.print_players(player)
                player.play_turn(self) # Pass a reference to this `Game` object
            turn += 1

    def print_players(self, current_player):
        """Prints all players (using the `Player.__str__` method), marking the current
        player with an arrow."""
        print()
        for player in self.players:
            print(f'{"->" if player==current_player else "  "} {player}')
        print()

    def declare_winner(self):
        """Checks if there's only one player left alive, and if so, prints a message declaring
        him/her the winner.

        Returns
        -------
        bool
            True when there's a winner, False if not.
        """
        pass # TODO!


# TODO: implement Player class
