For each of the listed data structures and each of the listed operations, state if applying the operation on the data structure is:
  - faster than O(n)
  - slower than O(n)
  - O(n)
  - not possible

If you don't know, you can do some experiments to find out!

list:
    add an element: 
    insert an element at the start: 
    remove an element with a specific value: 
    remove the last element: 
    remove the first element: 
    check if a certain value is contained: 

set:
    add an element: 
    insert an element at the start: 
    remove an element with a specific value: 
    remove the last element: 
    remove the first element: 
    check if a certain value is contained: 

tuple:
    add an element: 
    insert an element at the start: 
    remove an element with a specific value: 
    remove the last element: 
    remove the first element: 
    check if a certain value is contained: 

deque:
    add an element: 
    insert an element at the start: 
    remove an element with a specific value: 
    remove the last element: 
    remove the first element: 
    check if a certain value is contained: 

dict: (note! different operations)
    add a key+value pair: 
    get the value for a key: 
    get the key for a value: 
    remove a pair by its key: 
