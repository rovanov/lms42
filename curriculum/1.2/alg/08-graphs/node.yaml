name: Graphs
Description: Mazes, graphs and path finding.
days: 2
goals:
    graphs: 1

assignment:
    Graphs:
        - Today, we'll be solving mazes in different forms using different algorithms. Before we dive in, it's good to know that mazes are actually *graphs*, for which a lot of useful algorithms have been invented.
        -
            link: https://www.youtube.com/watch?v=eQA-m22wjTQ&list=PLDV1Zeh2NRsDGO4--qE8yH72HFL1Km93P&index=2
            title: Graph Theory Introduction
            info: What are graphs? How to represent graphs in code?

    Depth first maze solving:
        -
            link: https://www.youtube.com/watch?v=W9F8fDQj7Ok
            title: Search A Maze For Any Path - Depth First Search Fundamentals
            text: Solving a maze using a 'depth first search' by hand. You may want to skip the middle part of the video, as it is rather repetitive.
        -
            ^merge: feature
            text: |
                Solve the two mazes provided in `mazes.py` using depth first search in the file `dfs.py`. Diagonal moves are not allowed.
                Mark the found path using 'o' characters in the maze. Show what other squares have been visited using '.' characters.

                The output for the first maze should be something similar to this:
               
                <img src="dfs-example.png" style="border: 6px solid black;">
               
                *Hint:* You'll probably want to implement this recursively.

    Breadth first maze solving:
        - Now we'll move on the *breadth first search* to solve the same mazes. One advantage is that the first solution that we'll find will always be the shortest path!
        -
            link: https://www.techiedelight.com/depth-first-search-dfs-vs-breadth-first-search-bfs/
            title: Depth first search (DFS) vs Breadth first search (BFS)
            info: What's the difference between depth first and breadth first.
        -
            link: https://www.youtube.com/watch?v=oDqjPvD54Ss&list=PLDV1Zeh2NRsDGO4--qE8yH72HFL1Km93P&index=5
            title: Breadth First Search Algorithm | Shortest Path | Graph Theory
            info: Using breadth first search to find the shortest path in a graph.
        -
            link: https://www.youtube.com/watch?v=KiCBXu4P-2Y&list=PLDV1Zeh2NRsDGO4--qE8yH72HFL1Km93P&index=6
            title: Breadth First Search grid shortest path | Graph Theory
            info: "In case you can figure out what to do based on the above video: great, do it! If you need some more hints, this is where you need to be."
        
        -
            ^merge: feature
            text: |
                Solve the two mazes provided in `mazes.py` using breadth first search in the file `bfs.py`. This should 
                find the shortest path.
                Mark the found path using 'o' characters in the maze. Show what other squares have been visited using '.' characters.

                <img src="bfs-example.png" style="border: 6px solid black;">

                *Hint:* Recursion is no longer a good idea. Instead uses a loop retrieves elements from a
                queue (for which Python offers the `deque` data structure). Once you have reached the destination,
                you will need to work your way backwards to the origin to find the path. One way to do this is to
                maintain a dictionary that stores the previous square for each square that is put in the queue.

    A-star maze solving:
        - As you can see by the large number of '.' characters, doing a regular *breadth first search* causes a large portion of the maze to be explored. This can be really slow, for instance if your 'maze' is actually a map of Europe. The A* algorithm (pronounced 'A star') improves upon *breadth first search* by 'guiding' the exploration process in the right direction.
        - 
            link: https://www.youtube.com/watch?v=-L-WgKMFuhE
            title: "A* Pathfinding (E01: algorithm explanation)"
        -
            ^merge: feature
            text: |
                Solve the two mazes provided in `mazes.py` using A* search in the file `astar.py`. This should 
                find the shortest path, while exploring not nearly as many squares as *breadth first search* would. 
                Mark the found path using 'o' characters in the maze. Show what other squares have been visited using '.' characters.

                <img src="astar-example.png" style="border: 6px solid black;">

                *Hint:* Start by copying your *breadth first* implementation. The main thing you'll need to change is that
                the `deque` data structure is replaced by a `heapq`. Into this min heap, insert tuples where the first part
                is the estimated total distance for reaching a square. Later parts of the tuples can be any data you need 
                when eventually *visiting* a square, such as its location, its distance and the previous square on the 
                fastest path from the origin.

         
    Dijkstra maze solving:
        - |
            For the last part of this assignment, we'll look at a graph that is not shaped like a grid. Here's the graph:

            <img class="no-border" src="dijkstra.png">

            It represents the flight network for a budget airliner. Each (pink) vertex is an airport and each (green line) edge is a flight. The number in the middle of a line represents the price for that flight. These are the *weights* of the graph. Also note that this is an *undirected* graph, as flying from city 2 to city 3 is as expensive as flying back from city 3 to city 2 (both flights cost 11).
        - |
            Of course, being students, we're interested in the cheapest path between two cities. An algorithm to solve exactly this type of problem has been invented by [Edsger Dijkstra](https://en.wikipedia.org/wiki/Edsger_W._Dijkstra), a famous Dutch computer science scholar. The algorithm is quite similar to A*.
        -
            link: https://www.youtube.com/watch?v=pVfj6mxhdMw
            title: Graph Data Structure 4. Dijkstra’s Shortest Path Algorithm
            info: A video explanation.
        -
            link: https://www.freecodecamp.org/news/dijkstras-shortest-path-algorithm-visual-introduction/
            title: FreeCodeCamp - Dijkstra's Shortest Path Algorithm - A Detailed and Visual Introduction
            info: As the title says, this introduction is indeed *detailed*. But if you're struggling with the video, this text is probably helpful!
        -
            ^merge: feature
            text: |
                Write a program that finds the cheapest route between the city named *a* and the city named *s*. The above graph has already been converted to a Python
                data structure, in the form of an *adjacency list*, in the file `dijkstra.py`.

                *Hint:* Please try hard to implement this algorithm yourself, with as little peeking at example (pseudo) code as possible. You can do this! If you run into a problem, try to ask *specific* questions to your mentor or a teacher.
