use std::io::Read;
use show_image::{ImageInfo, make_window};

mod bit_reader;


fn main() {
    // Parse the command line arguments
    let mut args = std::env::args();
    args.next(); // The name of the application
    if args.len() != 1 {
        eprintln!("Usage: viewer <filename>");
        std::process::exit(1);
    }
    let filename = args.next().unwrap();

    // Read the file
    let data = get_file_as_byte_vec(&filename);
    let data_len = data.len();

    // Decode the image
    let start_time = std::time::Instant::now();
    let (width, height, pixels) = decode_image(data);

    // Brag about accomplishments
    println!("Decompressed {} kb of img42 data into {} kb of pixel data in {}s",
        data_len / 1024,
        pixels.len() / 1024,
        start_time.elapsed().as_millis() as f64 / 1000.0
    );

    // Create a window and display the image
    let window = make_window("image").expect("Couldn't create window");
    window.set_image((pixels, ImageInfo::rgb8(width, height)), "image-001")
        .expect("Couldn't display image");
    
    // Wait for the window to close
    while let Ok(_) = window.wait_key(std::time::Duration::from_secs(3600)) {}
}


/// Decodes/decompresses `data, and returns a tuple containing the image width,
/// image height and RGB pixel data.
fn decode_image(data: Vec<u8>) /* -> TODO */ {
    let mut br = bit_reader::BitReader::new(/* TODO */);

    // TODO!

    return (width, height, pixels);
}


/// Reads a file by the name of `filename` and returns a byte vector.
fn get_file_as_byte_vec(filename: &String) -> Vec<u8> {
    let mut f = std::fs::File::open(&filename).expect("File not found");
    let metadata = std::fs::metadata(&filename).expect("Unable to read metadata");
    let mut buffer = vec![0; metadata.len() as usize];
    f.read(&mut buffer).expect("Buffer overflow");

    buffer
}
