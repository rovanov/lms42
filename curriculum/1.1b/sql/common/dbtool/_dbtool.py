#!/bin/env python

import sys
import os
import subprocess
import random
import json
import time
import postgresqlite
import re
import socket
from collections import defaultdict


DBTOOL_DIR = os.path.dirname(__file__)
SOCKET_FILE = ".dbtool-execute.socket"
LOG_FILE = ".dbtool-execute.log"
LOG_QUERIES = 'logical' # 'real' / 'logical' / None


# Connect to our PostgreSQL database
init_db_cache = None
def init_db():
    global init_db_cache
    if not init_db_cache:
        config = postgresqlite.get_config()
        db = postgresqlite.connect(config=config)
        
        if LOG_QUERIES == 'real':
            db_execute_org = db.execute
            def db_execute_log(*args, **kwargs):
                print(">>", *args, end="\n\n", flush=True)
                return db_execute_org(*args, **kwargs)
            db.execute = db_execute_log
        
        env = os.environ | config.env
        type_map = {row['oid']: row['typname'].rstrip("0123456789") for row in db.execute('select oid,typname from pg_type')}
        init_db_cache = type('db_obj', (object,), {"db": db, "type_map": type_map, "env": env})
    return init_db_cache

def close_db():
    global init_db_cache
    init_db_cache = None


def time_to_seconds(s):
    if s.endswith('ms'):
        return float(s[:-2]) / 1000
    if s.endswith('s'):
        return float(s[:-1])
    raise ValueError("Time requires a 's' or 'ms' suffix")

def table_to_list(s):
    lines = s.split("\n")
    if lines[1].replace('-', '').replace('|', '').strip():
        raise ValueError(f"Invalid table header:\n{s}")
    results = []
    for line in lines[0:1] + lines[2:]:
        results.append([field.strip() if field.strip() != "*NULL*" else None for field in line.strip("| \t").split(' | ')])
    for index in range(len(results)-1):
        if len(results[index]) != len(results[index+1]):
            raise ValueError(f"Table has inconsistent column count at line {index+2}:\n{s}")
    return results

def list_to_table(l):
    result = "\n"
    for index, row in enumerate(l):
        result += "| " + " | ".join([str(field) if field!=None else "*NULL*" for field in row]) + " |\n"
        if index == 0:
            result += "| " + " | ".join(["---" for field in row]) + " |\n"
    return result

def str_to_list(s):
    return s.split(' ')

def list_to_str(l):
    return ' '.join(l)

def seconds_to_time_limit(s):
    unit = 's'
    if s < 0.5:
        s *= 1000
        unit = 'ms'
    s = to_upper_bound(s)
    return str(s) + unit


def check_equal(value,expected):
    if type(value) == type(expected) == list and len(value) == len(expected):
        for i in range(len(value)):
            if value[i] != expected[i] and expected[i] != '???':
                return f"expected {expected}, got {value}"
        
    elif value != expected and expected != '???':
        return f"expected {expected}, got {value}"

def check_equal_onordered(value,expected):
    return check_equal(sorted(value), sorted(expected))

def check_limit(value, limit):
    if value > limit:
        return f"expected <= {limit}, got {value}"

def row_to_strings(row):
    return [None if field==None else str(field) for field in row]

def check_results(value, expected):
    if len(value) != len(expected):
        return f"expected {len(expected)-1} rows, got {len(value)-1}"
    for index in range(len(value)):
        err = check_equal(row_to_strings(value[index]), expected[index])
        if err: return f"{err} at row {index}"

def check_unordered_results(value, expected):
    if len(value) != len(expected):
        return f"expected {len(expected)-1} rows, got {len(value)-1}"
    if value[0] != expected[0]:
        return f"expected columns {expected[0]}, got {value[0]}"

    expected_set = {str(row) for row in expected[1:]}

    for row in value[1:]:
        row = str(row_to_strings(row))
        if row not in expected_set:
            return f"unexpected row {row}"

def check_first_results(value, expected):
    return check_results(value[0:len(expected)], expected)

def to_upper_bound(num):
    num = round(num * 10)
    zeros = 0
    while num > 9:
        zeros += 1
        num //= 10
    num += 1
    return num * (10 ** zeros)


EXPECT_TYPES = dict(
    column_names = (str_to_list, list_to_str, check_equal,),
    column_types = (str_to_list, list_to_str, check_equal,),
    row_count = (int, str, check_equal,),
    modified_row_count = (int, str, check_equal,),
    column_count = (int, str, check_equal,),
    run_time = (time_to_seconds, seconds_to_time_limit, check_limit,),
    table_count = (int, str, check_equal,),
    plan_row_count = (int, to_upper_bound, check_limit,),
    tables = (str_to_list, list_to_str, check_equal_onordered,),
    results = (table_to_list, list_to_table, check_results,),
    unordered_results = (table_to_list, list_to_table, check_unordered_results,),
    first_results = (table_to_list, list_to_table, check_first_results,),
    query_count = (int, str, check_limit,),
    sql = (str, str, None,),
)


def parse_md(filename, up_to_line=None):
    with open(filename) as file:
        lines = file.readlines()
    result = defaultdict(dict)
    query_key = ""
    mode = None
    buffer = ""
    for line_number, line in enumerate(lines + [""]):
        if mode:
            if (line.startswith("```") if mode=="sql" else line.strip() == ""):
                if mode in result[query_key]:
                    raise ValueError(f"Duplicate '{mode}' in {query_key}")
                result[query_key][mode] = EXPECT_TYPES[mode][0](buffer.rstrip("\n"))
                mode = None
                buffer = ""
            else:
                buffer += line
        elif line.startswith("### "):
            line = line.strip("# \t\n\r")
            query_key = line.split(':',1)[0]
        elif line.startswith("```sql"):
            if up_to_line != None and line_number > up_to_line:    
                break
            mode = "sql"
        elif line.startswith("Expected ") and ":" in line:
            name, value = line[9:].split(':', 2)
            name = name.lower().replace(' ', '_')
            if name in EXPECT_TYPES:
                value = value.strip()
                if value == "": # a multi-line value, concluded by an empty line
                    mode = name
                else:
                    result[query_key][name] = EXPECT_TYPES[name][0](value)
            else:
                print(f"Invalid expect '{name}' in {query_key}")

    return result



def search_plan(plan, target_key):
    result = []
    if isinstance(plan, list):
        for item in plan:
            result += search_plan(item, target_key)
    elif isinstance(plan, dict):
        for k, v in plan.items():
            result += search_plan(v, target_key)
        if target_key in plan:
            result.append(plan[target_key])
    return result


def split_queries(sql):
    queries = [""]
    quote = None
    prev_ch = None
    for ch in sql:
        queries[-1] += ch
        if quote: # ignore chars up to quote
            if ch == quote:
                quote = None
        elif ch == ';': # end of query
            queries.append('')
        elif ch in ('"', "'"): # start a string/identifier
            quote = ch
        elif ch=='-' and prev_ch=='-': # comment until end of line
            quote = "\n"
        prev_ch = ch
    return [query.strip() for query in queries if query.strip() and not query.strip().startswith('--')]



class TransactionStack:
    def __init__(self):
        self.db = None

    def start(self):
        """Returns `True` if a new database connection was set up, or `False` when the
        connection was recycled (allowing reuse of the transaction cache)."""
        self.index = 0
        if self.db:
            try: # Test if the db connection is still valid
                self.db.execute("SELECT 1")
                return False
            except postgresqlite.pg8000.exceptions.InterfaceError:
                close_db()
        
        self.db = init_db().db
        self.index = 0
        self.saved_queries = []
        return True

    def _rollback_to(self, index):
        while index < len(self.saved_queries) and self.saved_queries[index] == None:
            index += 1
            if index >= len(self.saved_queries):
                return
        if index > 0:
            self.db.execute(f"ROLLBACK TO SAVEPOINT stack{index}")
        elif self.saved_queries:
            self.db.execute("ROLLBACK")

    def run(self, query, allow_cache=True):
            
        if self.index < len(self.saved_queries):
            if self.saved_queries[self.index] == query and allow_cache:
                self.index += 1
                if LOG_QUERIES == 'logical':
                    print('>> [cached]', query, end="\n\n", flush=True)
                return
            self._rollback_to(self.index)
            
            self.saved_queries = self.saved_queries[:self.index]

        if LOG_QUERIES == 'logical':
            print('>>', query, end="\n\n", flush=True)

        if self.index > 0:
            self.db.execute(f"SAVEPOINT stack{self.index}")
        else:
            self.db.execute("START TRANSACTION")

        self.index += 1
        self.saved_queries.append(query)

        try:
            result = execute_query(query)
        except Exception as e:
            self._rollback_to(self.index-1)
            self.saved_queries[-1] = None
            raise e

        if get_query_verb(query) == 'select':
            self.index -= 1
            self._rollback_to(self.index)
            self.saved_queries.pop()

        return result


class SQLError(Exception):
    pass


def execute_query(query):
    retries = 0
    while True:
        try:
            return init_db().db.execute(query)
        except Exception as e:
            msg = str(e)
            match = re.search("'M': '((\\\\.|[^'])+)'", msg)
            if match:
                msg = match.group(1)
            if msg == 'the database system is starting up' and retries < 10:
                retries += 1
                time.sleep(0.5)
            else:
                indented_query = "\t" + query.replace("\n","\n\t")
                msg = f"{msg}, for query:\n{indented_query}"
                raise SQLError(msg)


def get_query_verb(query):
    return re.split(r'\s', query, maxsplit=1)[0].lower()


def apply_sql_block_changes(sql, transaction_stack):
    for query in split_queries(sql):
        if get_query_verb(query) != 'select':
            transaction_stack.run(query)


def get_sql_block_results(sql, transaction_stack, expected=False):
    run_time = 0
    tresult = dict(query_count = 0, modified_row_count = 0, run_time = 0, table_count = 0, tables = [], plan_row_count = 0)
    qresult = {}

    for query in split_queries(sql):
        start_time = time.time()
        cursor = transaction_stack.run(query, allow_cache=False)
        tresult['query_count'] += 1
        tresult['run_time'] += float(time.time() - start_time)

        qresult = {}
        if cursor.description:
            rows = [list(row) for row in cursor]
            qresult['row_count'] = len(rows)
            qresult['column_count'] = len(cursor.description or [])
            qresult['column_names'] = [item[0] for item in cursor.description]
            qresult['column_types'] = [init_db().type_map.get(item[1],"text") for item in cursor.description]

            for col_index, col_type in enumerate(qresult['column_types']):
                if col_type == 'numeric':
                    for row in rows:
                        row[col_index] = round(float(row[col_index]), 4)

            if expected:
                ordered = "order by " in query.lower()
                if len(rows) >= 25:
                    if ordered:
                        qresult['first_results'] = [qresult['column_names']] + rows[0:10]
                elif ordered or len(rows) < 2:
                    qresult['results'] = [qresult['column_names']] + rows
                else:
                    qresult['unordered_results'] = [qresult['column_names']] + rows
            else:
                qresult['results'] = qresult['unordered_results'] = qresult['first_results'] = [qresult['column_names']] + rows
        else:
            if cursor.rowcount > 0:
                tresult['modified_row_count'] += cursor.rowcount

        if get_query_verb(query) in ("select", "insert", "update", "delete"):
            cursor.execute(f"EXPLAIN (FORMAT JSON) {query}")    
            plan = cursor.fetchone()[0]
            if plan:
                # print(query, json.dumps(plan,indent=4))
                tables = sorted(search_plan(plan, "Relation Name"))
                tresult['tables'] += tables
                tresult['table_count'] += len(tables)
                #tresult['plan_row_count'] += sum(search_plan(plan, "Plan Rows"))

    return qresult | tresult


def print_table(data, term_width):
    col_lens = [0] * len(data[0])
    for row in data:
        for col_num, value in enumerate(row):
            col_lens[col_num] = max(len(str(value)), col_lens[col_num])

    target_width = max(term_width, len(col_lens) * 6+1)
    while True:
        current_width = sum(col_lens) + len(col_lens)*3 + 1
        if current_width <= target_width:
            break
        idx = col_lens.index(max(col_lens))
        col_lens[idx] = int(col_lens[idx] * 0.8)

    print()
    for row_num, row in enumerate(data):
        for col_num, value in enumerate(row):
            l = col_lens[col_num] + 1
            value = str("NULL" if value==None else value)[0:l]
            print(f"| {value:<{l}}", end="|\n" if col_num+1==len(row) else "")
        if row_num == 0:
            for col_num, value in enumerate(row):
                l = col_lens[col_num] + 2
                print(f"|{'-'*l}", end="|\n" if col_num+1==len(row) else "")
    print()


def merge_alt_data(data, alt_data, add_missing_sections=False):
    for section, alt_info in alt_data.items():
        if not alt_info:
            continue

        if section not in data:
            if not add_missing_sections:
                continue
            data[section] = {}
        info = data[section]

        if 'sql' not in info:
            info['sql'] = "TODO"

        for label, expected in alt_info.items():
            if label != 'sql':
                info[label] = expected


def run_assignment(transaction_stack, specific_line=None, expected=False, term_width=100):
    todos = []
    errors = []
    oks = []

    global schema_modified
    schema_modified = False
    
    if specific_line != None:
        specific_line = int(specific_line)
    data = parse_md("assignment.md", up_to_line=specific_line)

    for alt_filename in ["_template/assignment.md", "_solution/assignment.md"]:
        try:
            merge_alt_data(data, parse_md(alt_filename), add_missing_sections = specific_line==None)
        except FileNotFoundError:
            pass

    last_section = list(data)[-1]

    for section, info in data.items():
        sql = data.get(section, {}).get('sql', '')
        if sql.lower() in ('','todo'):
            todos.append(section)
            continue
        print(f"\n### {section}")

        perform_checks = specific_line==None or section==last_section
        show_output = specific_line!=None and section==last_section

        try:
            if not perform_checks:
                apply_sql_block_changes(sql, transaction_stack)
                continue
            result = get_sql_block_results(sql, transaction_stack, expected)
        except SQLError as e:
            print(f"Section '{section}' has an SQL error: \u001b[31m{e}\u001b[37m", flush=True)
            errors.append(section)
            continue

        if show_output:
            if 'results' in result:
                data = result['results']
                if result['row_count'] > 100:
                    print(f"Showing first 50 out of {result['row_count']} rows")
                    data = data[0:50]
                print_table(data, term_width)
            affect = f" affecting {result['modified_row_count']} rows" if result['modified_row_count'] > 0 else ""
            print(f"Executed {result['query_count']} queries in {result['run_time']:.3f}s{affect}")

        if expected:
            for key, val in result.items():
                val = EXPECT_TYPES[key][1](val)
                print(f"Expected {key.replace('_',' ')}: {val}")
        else:
            ok = True
            for label, expect in info.items():
                if label == 'sql': continue
                err = EXPECT_TYPES[label][2](result[label], expect)
                if err:
                    ok = False
                    print(f"Section '{section}' failed on \u001b[31m{label.replace('_',' ')} ({err})\u001b[0m")
            if info and 'sql' not in info:
                print(f"Section '{section}' has no ```sql``` block")
            if ok:
                oks.append(section)
            else:
                errors.append(section)

    if specific_line == None and not expected:
        print()
        for label, items in {"OK": oks, "ERROR": errors, "TODO": todos}.items():
            if items:
                print(f"{label}:", ", ".join(items))

    if schema_modified or not os.path.exists("schema.md"):
        create_schema_md()



def create_schema_md():

    sql = """
    SELECT table_name, column_name, data_type, character_maximum_length, column_default, is_nullable
    FROM INFORMATION_SCHEMA.COLUMNS WHERE table_schema='public'
    ORDER BY table_name
    """

    db = init_db().db

    columns = defaultdict(list)
    for row in db.execute(sql):
        columns[row['table_name']].append([
            row['column_name'],
            row['data_type'] + (f"({row['character_maximum_length']})" if row['character_maximum_length'] != None else "") + ("" if row['is_nullable'] else " NOT NULL" ),
            row['column_default']
        ])


    sql = """
    SELECT (
        SELECT pc.relname
        FROM pg_class pc
        WHERE pc.oid=ct.conrelid
    ) table_name,
    conname AS key_name,
    pg_catalog.pg_get_constraintdef(oid, true) AS key_info
    FROM pg_catalog.pg_constraint ct
    WHERE connamespace='public'::regnamespace
    """
    keys = defaultdict(list)
    for row in db.execute(sql):
        keys[row["table_name"]].append([row['key_name'], row['key_info']])

    sys_out = sys.stdout
    try:
        with open("schema.md", "w") as out_file:
            sys.stdout = out_file
        
            for table, cols in columns.items():
                print(f"# {table}\n")
                print_table([['column name', 'type', 'default']]+cols, 1500)

                if keys[table]:
                    print_table([['key name', 'description']]+keys[table], 1500)

                print("\n")
    finally:
        sys.stdout = sys_out



def _run_as_daemon(daemon_callback, keep_fds=set(), change_cwd=True):
    # Do the Unix double-fork magic; see Stevens's book "Advanced
    # Programming in the UNIX Environment" (Addison-Wesley) for details
    pid = os.fork()
    if pid > 0:
        # Return the original process
        return

    # Decouple from parent environment
    if change_cwd:
        os.chdir("/")
    os.setsid()
    os.umask(0)

    keep_fds = {fd if type(fd)==int else fd.fileno() for fd in keep_fds}
    for file in [sys.stdout, sys.stderr, sys.stdin]:
        try:
            if file.fileno() not in keep_fds:
                file.close()
        except:
            pass
    for fd in range(1024):
        if fd not in keep_fds:
            try:
                os.close(fd)
            except:
                pass

    # Do second fork
    pid = os.fork()
    if pid > 0:
        # Exit from second parent; print eventual PID before exiting
        sys.exit(0)

    try:
        daemon_callback()
    except Exception as e:
        # Make sure an exception is not caught by _run_as_daemon's caller
        print(traceback.format_exc(), file=sys.stderr, flush=True)
    sys.exit(0)


already_reset = False


def run_transaction_server(server, as_daemon=False):

    if as_daemon:
        sys.stdout = sys.stderr = log_fd = open(LOG_FILE, 'a', buffering=1)
    else:
        log_fd = sys.stderr

    transaction_stack = TransactionStack()

    print("Transaction server started accepting connections..")

    while True:
        try:
            conn, addr = server.accept()
        except TimeoutError:
            break

        if transaction_stack.start():
            Commands.reset(True)

        options = json.loads(conn.makefile("r").readline())
        print("Request:", options)

        sys.stdout = sys.stderr = conn.makefile("w", buffering=1) # print() output should go to the client

        run_assignment(transaction_stack=transaction_stack, **options)

        sys.stdout.close()
        conn.close()
        sys.stdout = sys.stderr = log_fd

        print("Request handled")

    print("Exiting transaction server due to timeout")
    os.remove(SOCKET_FILE)
    sys.exit(0)


def get_term_width():
    try:
        return os.get_terminal_size().columns
    except:
        return 1500


class Commands:

    @staticmethod
    def client():
        """Run the PostgreSQL client."""
        Commands.reset(True)
        try:
            subprocess.run(["psql"], env=init_db().env)
        except FileNotFoundError:
            print("The psql client is missing.")
            sys.exit(1)

    @staticmethod
    def expected(specific_line=None):
        """Generate 'Expected' clauses based on the SQL blocks in ./assignment.md."""
        Commands.execute(specific_line, expected=True)

    @staticmethod
    def execute(specific_line=None, expected=False):
        """Run all SQL blocks in ./assignment.md, checking if any Expected-clauses are satisfied. When a line number is given, 
        the output for the SQL block containing that line is shown (and no further SQL blocks are run)."""
        transaction_stack = TransactionStack()
        if transaction_stack.start():
            Commands.reset(True)

        run_assignment(transaction_stack, specific_line, expected, get_term_width())

    @staticmethod
    def execute_cached(specific_line=None, expected=False):
        """Run all SQL blocks in ./assignment.md, checking if any Expected-clauses are satisfied. When a line number is given, 
        the output for the SQL block containing that line is shown (and no further SQL blocks are run)."""

        client = socket.socket(socket.AF_UNIX, socket.SOCK_STREAM)
        connected = False
        try:
            client.connect(SOCKET_FILE)
            connected = True
        except (FileNotFoundError, ConnectionRefusedError):
            pass

        if not connected:
            Commands.transaction_server(as_daemon=True)
            client.connect(SOCKET_FILE)

        client.send((json.dumps({
            "expected": expected,
            "specific_line": specific_line,
            "term_width": get_term_width(),
        })+"\n").encode('utf8'))

        while True:
            out = client.recv(1024)
            if not out: break
            print(out.decode('utf-8'), end='', flush=True)
        
        client.close()


    @staticmethod
    def run(*args):
        """Run the given command with the proper environment variables for PostgreSQLite"""
        Commands.reset(True)

        if not args:
            print("A command (and optionally arguments) must be specified.")
        else:
            subprocess.run(args, env=init_db().env)


    @classmethod
    def reset(cls, only_when_empty=False):
        """Reset to template database"""

        db = init_db().db
        has_tables = db.execute("select 1 from pg_tables where schemaname='public' limit 1").fetchone()

        global already_reset
        if (only_when_empty and has_tables) or already_reset:
            return
        already_reset = True

        if has_tables:
            print("Dropping all database objects...")
            # Kill any other connections, as they may have locks on our data objects.
            # From https://tableplus.com/blog/2018/04/postgresql-how-to-kill-all-other-active-connection-to-database.html
            db.execute("""
            SELECT pg_terminate_backend(pg_stat_activity.pid)
            FROM pg_stat_activity
            WHERE pid <> pg_backend_pid()
            """)
            for what in ['view', 'table', 'sequence']:
                for row in db.execute(f"select {what}name as name from pg_{what}s where schemaname='public'"):
                    db.execute(f"drop {what} {row['name']} cascade")
            db.commit()

        if os.path.exists("create_db.sql") or os.path.exists("create_db.py"):
            print("Creating template database...")

        if os.path.exists("create_db.sql"):
            try:
                result = subprocess.run(["psql", "-bq", "--command=\\set ON_ERROR_STOP true", "--file=create_db.sql"], env=init_db().env, stdout=subprocess.DEVNULL)
            except FileNotFoundError:
                print("Please install the 'postgresql-libs' Manjaro package.")
                sys.exit(1)

            if result.returncode:
                sys.exit(1)

        if os.path.exists("create_db.py"):
            sys.path = ['.'] + sys.path
            import create_db
            random.seed(42)
            create_db.run(db)
            db.commit()
            random.seed()


    @staticmethod
    def transaction_server(as_daemon=False):
        """Run the server process (that executes SQL blocks using cached transaction savepoints) in the foreground. This is for debugging purposes. It is normally started automatically in the background."""

        print("Starting transaction server...")

        if os.path.exists(SOCKET_FILE):
            os.remove(SOCKET_FILE)

        server = socket.socket(socket.AF_UNIX, socket.SOCK_STREAM)
        server.bind(SOCKET_FILE)
        server.settimeout(2*60*60) # 2 hour timeout
        server.listen(1)

        if as_daemon:
            close_db()
            _run_as_daemon(lambda: run_transaction_server(server, as_daemon=True), keep_fds={server}, change_cwd=False)
        else:
            run_transaction_server(server)


if __name__ == '__main__':
    cmd = sys.argv[1] if len(sys.argv)>1 else ""
    func = getattr(Commands, cmd, '')
    if func:
        func(*sys.argv[2:])
    else:
        print(f"Invalid command '{cmd}'. Commands are:")
        for name in dir(Commands):
            if not name.startswith('__'):
                func = getattr(Commands, name)
                print(f"- {name}: {func.__doc__}")
        sys.exit(1)
