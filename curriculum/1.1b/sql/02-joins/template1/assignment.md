## Joining two tables

Within Visual Studio code, press `ctrl-shift-b` to open the PostgreSQL client and connect it to the provided database.

### 1.1
For each store, display its name and the first and last name of its manager.
```sql
TODO
```

Expected unordered results: 
| name | first_name | last_name |
| --- | --- | --- |
| Sakila Super Store Lethbridge | Mike | Hillyer |
| Sakila Outlet Woodridge | Jon | Stephens |

Expected tables: staff store

### 1.2

For each store, display its *city* and the first and last name of its manager.
```sql
TODO
```

Expected unordered results: 
| city | first_name | last_name |
| --- | --- | --- |
| Lethbridge | Mike | Hillyer |
| Woodridge | Jon | Stephens |


### 1.3

For each staff member, display the city where he lives and the city of the store where he works.

```sql
TODO
```

Expected unordered results: 
| city | city |
| --- | --- |
| Lethbridge | Lethbridge |
| Woodridge | Woodridge |
| Oxon | Lethbridge |


### 1.4

Like the previous query, but name the output columns `staff_city` and `store_city` respectively, to prevent confusion. *Hint:* You'll need to use `as`.

```sql
TODO
```

Expected unordered results: 
| staff_city | store_city |
| --- | --- |
| Lethbridge | Lethbridge |
| Woodridge | Woodridge |
| Oxon | Lethbridge |


## Joining multiple tables

### 2.1

For each rental, ordered by rental time, show the rental time and the title of the film that was rented.

*Hint:* In addition to the `rental` and `film` tables, you will need to use the `inventory` table. This table holds a row for each physical DVD that the rental company owns. There can be more than one for a given film.

```sql
TODO
```

Expected row count: 15824
Expected tables: film inventory rental
Expected first results: 
| rental_time | title |
| --- | --- |
| 2005-05-24 22:53:30 | Blanket Beverly |
| 2005-05-24 22:54:33 | Freaky Pocus |
| 2005-05-24 23:03:39 | Graduate Lord |
| 2005-05-24 23:04:41 | Love Suicides |
| 2005-05-24 23:05:21 | Idols Snatchers |
| 2005-05-24 23:08:07 | Mystic Truman |
| 2005-05-24 23:11:53 | Swarm Gold |
| 2005-05-24 23:31:46 | Lawless Vision |
| 2005-05-25 00:00:40 | Matrix Snowman |
| 2005-05-25 00:02:21 | Hanging Deep |

### 2.2

For each rental, ordered by rental time, show the first and last name of the customer and the title of the film that was rented.

```sql
TODO
```

Expected row count: 15824
Expected tables: customer film inventory rental
Expected first results: 
| first_name | last_name | title |
| --- | --- | --- |
| Charlotte | Hunter | Blanket Beverly |
| Tommy | Collazo | Freaky Pocus |
| Manuel | Murrell | Graduate Lord |
| Andrew | Purdy | Love Suicides |
| Delores | Hansen | Idols Snatchers |
| Nelson | Christenson | Mystic Truman |
| Cassandra | Walters | Swarm Gold |
| Minnie | Romero | Lawless Vision |
| Ellen | Simpson | Matrix Snowman |
| Danny | Isom | Hanging Deep |


### 2.3

Show the titles of all films that have an actor with the first name of 'Sean'.

```sql
TODO
```

Expected row count: 59
Expected tables: actor film film_actor
Expected first results: 
| title |
| --- |
| Ace Goldfinger |
| Alamo Videotape |
| Arabia Dogma |
| Brooklyn Desert |
| Chinatown Gladiator |
| Crusade Honey |
| Darn Forrester |
| Divorce Shining |
| Dracula Crystal |
| Dumbo Lust |

### 2.4

Show the first and last names of all actors that have played in at least one film in the *Horror* category, ordered by first and last name.

```sql
TODO
```

Expected row count: 199
Expected tables: actor category film film_actor film_category
Expected first results: 
| first_name | last_name |
| --- | --- |
| Adam | Grant |
| Adam | Hopper |
| Al | Garland |
| Alan | Dreyfuss |
| Albert | Johansson |
| Albert | Nolte |
| Alec | Wayne |
| Angela | Hudson |
| Angela | Witherspoon |
| Angelina | Astaire |


### 2.5

Show the titles of all films that are in the *Horror* category as well as the *Sci-Fi* category, ordered by title.

```sql
TODO
```

Expected row count: 29
Expected tables: category category film film_category film_category
Expected first results:     
| title |                 
| --- |           
| Alabama Devil |
| Arachnophobia Rollercoaster |
| Ballroom Mockingbird |
| Basic Easy |         
| Bringing Hysterical |
| Carrie Bunch |       
| Chitty Lock |          
| Cleopatra Devil |          
| Crossroads Casualties |


## Counting

### 3.1

Count the number of customers.

```sql
TODO
```

Expected results: 
| count |
| --- |
| 599 |

### 3.2

Count the number of unique first names for customers.

```sql
TODO
```

Expected results: 
| count |
| --- |
| 591 |

### 3.3

Count the total number of rentals for all customers who's first name is *Kelly*.

```sql
TODO
```

Expected tables: customer rental
Expected results: 
| count |
| --- |
| 47 |

### 3.4

Count the number of films in the category *Drama* or the category *Foreign* (or both, in which case the film should be counted just once), and the number of different lengths that these films have.

```sql
TODO
```

Expected results:              
| film_count | length_count |                                       
| --- | --- |                                 
| 366 | 129 |   

## Left joins

### 4.1

Show all columns for all rentals and associated payments of the inventory item with id `1801`. A result row should be displayed even when no payment has been made for a given rental.

```sql
TODO
```

Expected row count: 5
Expected tables: payment rental
Expected column names: id rental_time inventory_id customer_id return_time staff_id id customer_id staff_id rental_id amount time

### 4.2

Show the first and last names for all customers that have never rented a video.

```sql
TODO
```

Expected unordered results: 
| first_name | last_name |
| --- | --- |
| Gwendolyn | May |
| Shirley | Allen |
| Katie | Elliott |
| Elaine | Stevens |
| Rachel | Barnes |
| Herman | Devore |
| Billy | Poulin |
| Byron | Box |

### 4.3

For all films for which the title starts with a `U`, display a row for each of its special features, containing the title of the film and the name of the feature. If a film has no special features, a single row should be shown, with the special feature name being `NULL`.

```sql
TODO
```

Expected unordered results: 
| film_title | feature_name |
| --- | --- |
| Unfaithful Kill | Deleted Scenes |
| Unforgiven Zoolander | Trailers |
| Unforgiven Zoolander | Behind the Scenes |
| United Pilot | Trailers |
| Untouchables Sunrise | Trailers |
| Untouchables Sunrise | Deleted Scenes |
| Usual Untouchables | Behind the Scenes |
| Uncut Suicides | *NULL* |
| Uprising Uptown | *NULL* |
| Uptown Young | *NULL* |
| Undefeated Dalmations | *NULL* |
| Unbreakable Karate | *NULL* |
