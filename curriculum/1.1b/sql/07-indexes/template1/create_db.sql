create table people(
    id serial, -- primary key,
    first_name text not null,
    last_name text not null,
    gender char(1) not null,
    birthdate date,
    join_date date,
    boss_id int --references people(id)
);

create table departments(
    id serial, -- primary key,
    name text not null
);

create table people_departments(
    person_id integer not null,-- references people(id),
    department_id integer not null-- references departments(id)
    --primary key(person_id, department_id)
);
