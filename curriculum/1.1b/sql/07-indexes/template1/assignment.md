### 1.1

Write a query that counts the number of people with either `Lisa` or `Martin` as their first name.

```sql
TODO
```

### 1.2

- Use the output of `EXPLAIN` to describe in your own words how PostgreSQL executes to above query.
- How long did the query take to run?
- How long would you expect the query to take if there were 10 times as many people in the database?

### 1.3

Use `CREATE INDEX` to add an index to the `people` table that speeds up your query.

```sql
TODO
```

Make sure to give your index a sensible name.

### 1.4

Copy-paste your query from 1.1 here. It should be faster now!

```sql
TODO
```

### 1.5

- Use the output of `EXPLAIN` to describe in your own words how PostgreSQL executes to query this time.
- How long did the query take to run?
- How long would you expect the query to take if there were 10 times as many people (including 10 times as many Lisas and Martins) in the database?

### 1.6

Copy-paste your query from 1.1 here again, but modify it such that it searches for people called `Truus` or `Henk`.

```sql
TODO
```

### 1.7

- How long did the query take to run?
- Why is there a difference with the time taken by 1.4?
- How long would you expect the query to take if there were 10 times as many people (assuming still no `Truus`es nor `Henk`s) in the database?



### 2.1

