## INSERT

### 1.1

Insert a new category called *Millennium* into the database, letting PostgreSQL autogenerate the `id` for it. In a second query, show the name of the category with the highest `id`.

```sql
TODO
```

Expected query count: 2
Expected modified row count: 1
Expected results: 
| name |
| --- |
| Millennium |


### 1.2

Insert a new actor called *Matthew McConaughey* into the database. In a second query, show the full row for all actors with last name *McConaughey*.

```sql
TODO
```

Expected row count: 1
Expected column names: id first_name last_name
Expected query count: 2
Expected modified row count: 1


### 1.3

Insert a new film into the database with name *Interstellar*, release year 2014, length 169, rating *PG-13*, rental duration 3, rental rate 4.99, replacement value 20.99. The description should be as follows: `A team of explorers travel through a wormhole in space in an attempt to ensure humanity's survival.`

In a second query, show the full row for the film with the highest `id`.

```sql
TODO
```

Expected query count: 2
Expected modified row count: 1
Expected results: 
| id | title | description | release_year | rental_duration | rental_rate | length | replacement_cost | rating |
| --- | --- | --- | --- | --- | --- | --- | --- | --- |
| ??? | Interstellar | A team of explorers travel through a wormhole in space in an attempt to ensure humanity's survival. | 2014 | 3 | 4.99 | 169 | 20.99 | PG-13 |


### 1.4

Insert *Matthew McConaughey* as an actor for the film *Interstellar*. Remember that you cannot use literal `id` value in your queries, so you will need to use `INSERT INTO ... SELECT` or a number of subqueries.

As a second query, show the first names of all actors in *Interstellar*.

```sql
TODO
```

Expected query count: 2
Expected modified row count: 1
Expected results: 
| first_name |
| --- |
| Matthew |


### 1.5

Add the film Interstellar to the inventory of the *Sakila Super Store Lethbridge*. Again: there should be no literal ids in your query!

```sql
TODO
```

Expected modified row count: 1
Expected tables: film inventory store


### 1.6

Insert a new staff member named *Bob Alisson*, working at the store at *28 PostgreSQL Boulevard*, into the database. You can make up the rest of his personal information.

```sql
TODO
```

Expected modified row count: 1
Expected tables: staff store


## UPDATE

### 2.1

Update staff member *Bob Alisson*'s email address to `bob@vhs-forever.com`. Write a second query that shows the email addresses and the store id for all staff members.

```sql
TODO
```

Expected unordered results: 
| email | store_id |
| --- | --- |
| Mike.Hillyer@sakilastaff.com | 1 |
| Jon.Stephens@sakilastaff.com | 2 |
| Han.Solo@sakilastaff.com | 1 |
| bob@vhs-forever.com | 2 |


### 2.2

Use a single query to move all films with id 970 and higher to the store with id 2. Write a second query that shows the id and the number of items in the inventory for all stores.

```sql
TODO
```

Expected query count: 2
Expected modified row count: 149
Expected unordered results: 
| store_id | count |
| --- | --- |
| 1 | 2189 |
| 2 | 2393 |


### 2.3

Update all customers with first or last name *Morris* so that they are a customer of the store with id 1.

```sql
TODO
```

Expected modified row count: 2


### 2.4

Update all payments conducted on the 14th of February 2007, so that everyone has a 50 cent discount. *Hint:* the `time` column contains a full timestamp, including the date. Use the `DATE` function to extract just the date from it.

```sql
TODO
```

Expected modified row count: 25


## DELETE

### 3.1

Remove the *New* category (id 13) association from all the films that have it. Note that you should *not* remove the film or the category, only the link between the two. You *are* allowed to just use a literal id for the category this time.

```sql
TODO
```

Expected modified row count: 190


### 3.2

Remove the category with the name *New*.

```sql
TODO
```

Expected modified row count: 1


### 3.3

Delete all rentals that have been rented out on 30th of May on 2005.

```sql
TODO
```

Expected modified row count: 155



## Subqueries


### 4.1

Show the first and last names for all customers that have the same first name as any of the staff members. Use the `IN` keyword and a subquery for this. Do *not* use `JOIN`.

```sql
TODO
```

Expected row count: 3
Expected tables: customer staff


### 4.2

For all inventory items that are film that have the word *bug* in their titles, set the store id to 2.

```sql
TODO
```

Expected modified row count: 4582


### 4.3

Show the average length of all films with a length longer than the overall average film length.

```sql
TODO
```

Expected tables: film film
Expected results: 
| avg |
| --- |
| 150.9163 |


### 4.4

Show the titles of all films that have been released in the same year as the latest 'G' rated film where 'Sean Guiness' was an actor in, ordered by film title.

```sql
TODO
```

Expected row count: 22
Expected first results: 
| title |
| --- |
| Affair Prejudice |
| Bill Others |
| Chaplin License |
| Clockwork Paradise |
| Darn Forrester |
| Effect Gladiator |


### 4.5

Find the movie buffs (the binge watchers). Display the first and last name and the number of rentals for each customer that has rented more films than the average. Sort the results by the descending number of rentals, and then by ascending last name.

```sql
TODO
```

Expected row count: 286
Expected tables: customer rental rental
Expected first results: 
| first_name | last_name | rental_count |
| --- | --- | --- |
| Eleanor | Hunt | 46 |
| Karl | Seal | 45 |
| Marcia | Dean | 42 |
| Clara | Shaw | 42 |
| Tammy | Sanders | 41 |
| Wesley | Bull | 39 |
| Tim | Cary | 39 |
| Marion | Snyder | 39 |
| Daisy | Bates | 38 |
| Elizabeth | Brown | 38 |


### 4.6

Relabel the millennium movies. Add the category *Millennium* (in film_category) to all the films that have no category yet and have a release year greater than 1999.

You may do this with either `JOIN`s, subqueries, or both.

Next, write a second query that show the title of all films in the *Millennium* category.

```sql
TODO
```

Expected query count: 2
Expected modified row count: 3
Expected unordered results: 
| title |
| --- |
| West Lion |
| Interstellar |
| Effect Gladiator |
