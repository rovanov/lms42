## It's a GROUP effort

Within Visual Studio code, press `ctrl-shift-b` to open the PostgreSQL client and connect it to the provided database.

### 1.1

Show the genre id and total number of games for each genre.

```sql
TODO
```

Expected tables: game
Expected unordered results: 
| genre_id | count |
| --- | --- |
| 11 | 1366 |
| 9 | 806 |
| 3 | 605 |
| 5 | 580 |
| 4 | 1314 |
| 10 | 714 |
| 6 | 492 |
| 2 | 1038 |
| 7 | 761 |
| 12 | 578 |
| 1 | 1900 |
| 8 | 1206 |

### 1.2

Show the platform id and the average, minimum and maximum number of sales per platform.

```sql
TODO
```

### 1.3

Show the publisher id and number of published games for each publisher, ordered by number of published games, starting with the highest number.

```sql
TODO
```

Expected row count: 577
Expected first results: 
| publisher_id | count |
| --- | --- |
| 352 | 774 |
| 369 | 662 |

### 1.4

Show the release year and the amount of games published per release year for the platform with id `5`, ordered by release year.

```sql
TODO
```

Expected row count: 12
Expected first results: 
| release_year | count |
| --- | --- |
| 2005 | 18 |
| 2006 | 93 |


## Mind if i JOIN you?

### 2.1

Show the genre name and total number of games for each genre, ordered by genre name.

```sql
TODO
```
Expected row count: 12
Expected first results:
| genre_name | count |                   
| --- | --- |                 
| Action | 1900 |       
| Adventure | 1038 |   


### 2.2

Show the platform name and total number of sales (over all regions) per platform, ordered by platform name.

```sql
TODO
```

Expected row count: 31
Expected first results: 
| platform_name | sum |
| --- | --- |
| 2600 | 22852 |
| 3DO | 1008 |
| 3DS | 132500 |
| DC | 5720 |
| DS | 157842 |
| GB | 3104 |
| GBA | 301692 |
| GC | 79132 |
| GEN | 3240 |
| GG | 272 |


### 2.3

Show the release year and the number of games released for any of the Playstation platforms in that release year, ordered by release year.

The codes for all Playstation platforms start with the letters `PS`. When in a given year a game is released for `PS3` as well as `PS4`, we should still only count it once.

```sql
TODO
```

Expected row count: 24
Expected first results: 
| release_year | count |
| --- | --- |
| 1994 | 17 |
| 1995 | 99 |
| 1996 | 164 |
| 1997 | 188 |


### 2.4

Show the name of the region with the highest number of sales for (all platforms of) the game 'Half-Life 2', as well as the number of sales in that region.

```sql
TODO
```

Expected tables: game game_platform game_publisher region region_sales
Expected results: 
| region_name | sales |
| --- | --- |
| North America | 2660 |


## I will have what you are HAVING (todo: WHERE!)

### 3.1

Show the names of all games that have more than one publisher.

```sql
TODO
```

Expected row count: 341
Expected column names: game_name


### 3.2

Show the platform name and the total number of sales (all games and all regions) for each platform that has at least a total number of sales of `500000`.

```sql
TODO
```

Expected tables: game_platform platform region_sales
Expected unordered results: 
| platform_name | sales |
| --- | --- |
| DS | 856214 |
| PS2 | 1271704 |
| PS3 | 973085 |
| PS | 749025 |
| Wii | 932826 |
| X360 | 994206 |


### 3.3

Like above, but now only consider sales of games that were released in or before the year 2005.

Expected unordered results: 
| platform_name | sales |
| --- | --- |
| PS2 | 991574 |
| PS | 749025 |


### 3.4

Show the years (in descending order) in which less than 15 publisher have released a game

```sql
TODO
```

Expected tables: game_platform
Expected results: 
| release_year |
| --- |
| 2020 |
| 2017 |
| 1985 |
| 1984 |
| 1980 |
