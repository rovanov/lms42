name: Aggregate functions
goals:
    aggregate: 1
resources:
    - 
        link: https://www.youtube.com/watch?v=jcoJuc5e3RE
        title: Basic Aggregate Functions in SQL (COUNT, SUM, AVG, MAX, and MIN)
        info: Learn how to use SQL's basic aggregation functions like COUNT, SUM, AVG, MAX, and MIN in this step-by-step tutorial!
    -
        link: https://www.sqlitetutorial.net/sqlite-group-by/
        title: SQLite Group By
        info: Practical examples of how to use `GROUP BY` and aggregate functions in SQLite.
    - 
        title: "SQLite Having"
        link: https://www.sqlitetutorial.net/sqlite-having/
        info: How the results of `GROUP BY` can be filtered using `HAVING`.
    -
        link: https://www.youtube.com/watch?v=nNrgRVIzeHg
        title: Advanced Aggregate Functions in SQL (GROUP BY, HAVING vs. WHERE)
        info: In this step-by-step video tutorial, you will learn some more advanced SQL aggregation concepts by using the GROUP BY, HAVING, and WHERE clauses!
assignment:
    Assignment:
    - |
        In databases, data aggregation means that we apply an _aggregation function_ to rows to form a single value. Aggregation functions are built-in functions (in the database) that can perform some computation, like for example compute the sum of a list of numbers. Other aggregation functions are average, count, min and max.

        In other words we do not look at individual rows, but take many rows together and summarize data. 

    - Study the database schema: |
        To help you get an understanding of the database structure an overview of the tables and their relation is given. This should help you construct your queries. In a later lesson we will discuss database schemas in more depth. For now it is enough to know which tables are present in the database and which relationships exist between theses tables (by looking at the lines between these tables).
        
        Note that the lines connect specific attributes between two tables. For example the line between the `genre` and the `game` table connect the `id` attribute from the `genre` table with the `genre_id` attribute from the `game` table.

        ![](overview-data-video-games.png)

    - SQL coding conventions: |
        When using aggregate functions, please write your queries in the following form: 
            
        ```sql
        SELECT 
            t.first_name, 
            t.last_name,
            s.name, 
            COUNT(p.id) AS count_projects
        FROM teacher t
        JOIN skill s ON s.teacher_id = t.id
        JOIN project p ON p.teacher_id = t.id
        WHERE t.level = 'Awesome'
            AND t.years_experience > 3
            AND s.name LIKE '%Programming%'
        GROUP BY t.teacher_id
        HAVING count_projects > 3
        ORDER BY t.first_name
        LIMIT 2
        ```

        So, in addition to the conventions mentioned in the previous assignment:
        * You should put the `GROUP BY` and `HAVING` clauses on a new line for clarity.
        * You may put `SELECT` parameters below each other for clarity, if the line would become very long otherwise.

    - Important notes: |        
        - For each of the questions below, we expect your answer to be a *single* query, unless stated otherwise.
        - Your queries should not contain literal ids or other information that was not given in the question. So when the assignment says to show all info for *Frank*, you can't do something like `SELECT * FROM teachers WHERE id=42` but you need to do something like `SELECT * FROM teachers WHERE name='Frank'`.

    It's a GROUP effort:
    -
        ^merge: queries

    Mind if I JOIN you?:
    -
        ^merge: queries

    I will have what you are HAVING:
    -
        ^merge: queries
