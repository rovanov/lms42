with open('solution.sql') as file:
    lines = file.readlines()

with open('assignment-template.md', 'w') as template, open('assignment-solution.md', 'w') as solution: 

    h1 = 0
    h2 = 0
    for line in lines:
        line = line.rstrip()
        if line == "":
            pass
        elif line.startswith("-- "):
            if h2:
                solution.write("```\n")
            if line[3].isdigit():
                h2 += 1
                header = f"\n### {h1}.{h2}\n"
                question = line.split('.', 1)[1].strip()
                solution.write(f"{header}```sql\n")
                template.write(f"{header}\n{question}\n\n```sql\nTODO\n```\n")
            else:
                template.write(f"\n\n## {line[3:].strip(' :')}\n\n")
                h1 += 1
                h2 = 0
        else:
            solution.write(f"{line}\n")
    if h2:
        solution.write("```\n")
