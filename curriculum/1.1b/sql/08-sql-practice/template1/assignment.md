
## Create a database

### 1: ERD

```plantuml

' Some settings to make your ERD look better
hide circle
skinparam linetype ortho

entity TODO {}
```

### 2: Import into temporary table

```sql
TODO
```

### 3: Create schema

```sql
TODO
```

### 4: Populate schema

```sql
TODO
```

## Insert more data


### 5.1

Insert 3 new agents into the database. You are free to choose their name and other attributes.

```sql
TODO
```

### 5.2

Insert suspect 'Dr. Jekyll' into the database and add him to all incidents associated with 'Mr. Hyde'.

```sql
TODO
```

### 5.3

Insert the following incident into the database: A suspect has caused a riot (10-34) at location '425 Phillips Pine'. This incident was reported by agent 'Agent Smith'.

```sql
TODO
```


## Query the data

### 6.1

Display the list of unique police code (or incident_type) for the city of 'Barbarahaven'.

```sql
TODO
```

### 6.2

Create a VIEW to display the daily number of incidents.

```sql
TODO
```

### 6.3

Display the number of bomb threats (police code 10-89) handled by agent 'Agent Smith'.

```sql
TODO
```

### 6.4

Display the city with the most incidents. Show the city and number of incidents.

```sql
TODO
```

### 6.5

Create a VIEW to display the number of incidents and the total amount fined for every agent in the database. Sort the data by total amount fined.

```sql
TODO
```

### 6.6

Display all suspects that are involved in more than the average number of incidents (per suspect). Show the suspect's name and the number of incidents he or she is involved in.

```sql
TODO
```


## Change the data

### 7.1

Suspect 'John Doe' turned out to be an accomplice in a different incident. Update the database so that 'John Doe' is no longer a suspect in incident 1458 but on incident 1421.

```sql
TODO
```

### 7.2

Due to an administration error all fines that have been given to traffic violations have been registered incorrectly. Update the fines for all incidents with police code 10-58. Reduce all amounts by 100.

```sql
TODO
```

### 7.3

It appears that suspect 'Dr. Jekyll' is the same person as 'Mr. Hyde'. Update the database so that the incidents associated with the two suspects are associated with one suspect named 'Dr. Jekyll-Hyde'.

```sql
TODO
```


## Make it fast

### 8.1

```sql
TODO
```


## Extend the design

### 9

TODO: copy and modify plantuml
