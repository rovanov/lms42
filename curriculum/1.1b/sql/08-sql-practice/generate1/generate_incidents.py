from random import randint, gauss
from faker import Faker
import csv

# Constants
NUM_AGENTS = 300
NUM_INCIDENTS = 15000
MAX_AGENTS_PER_INCIDENT = 3
MAX_SUBJECT_PER_INCIDENT = 4
JOHN_INCIDENT = 1458
NOT_JOHN_INCIDENT = 1421
JOHN_NAME = "John Doe"

def read_police_codes():
    with open('police_codes.csv') as csv_file:
        result = []
        csv_reader = csv.reader(csv_file, delimiter=';')
        line_count = 0
        for row in csv_reader:
            if line_count == 0:
                line_count += 1
            else:
                result.append({ 'code': row[0], 'description': row[1] })
    return result

def generate_agents():
    result = []
    faker = Faker()
    for _ in range(NUM_AGENTS):
        badge = [str(randint(0, 9)) for i in range(6)]
        result.append({'name': faker.name(), 'badge': "".join(badge)})
    # Insert mandatory agent
    badge = [str(randint(0, 9)) for i in range(6)]
    result.append({'name': 'Agent Smith', 'badge': "".join(badge)})
    return result
    
def generate_suspects():
    result = []
    faker = Faker()
    for i in range(int(NUM_INCIDENTS / 3)):
        prefix = ''
        gender = ''
        if i % 2:
            if i == randint(0, NUM_INCIDENTS):
                prefix = faker.prefix_female() + " "
            name = prefix + faker.name_female() 
            gender = 'female'
        else:
            if i == randint(0, NUM_INCIDENTS):
                prefix = faker.prefix_male() + " "
            name = prefix + faker.name_male()
            gender = 'male'
        result.append({
            'name': name, 
            'gender': gender, 
            'date_of_birth' : faker.date_between(start_date='-60y', end_date='-20y') if randint(0,9)>2 else ''
        })
    # Insert mandatory suspects
    result.append({
        'name': 'Mr. Hyde', 
        'gender': 'male', 
        'date_of_birth' : faker.date_between(start_date='-60y', end_date='-20y')
    })
    result.append({
        'name': JOHN_NAME, 
        'gender': 'male', 
        'date_of_birth' : faker.date_between(start_date='-60y', end_date='-20y')
    })
    return result

def generate_addresses():
    result = []
    faker = Faker()
    num_address = int(NUM_INCIDENTS / 3)
    fixed_cities = [randint(0, num_address) for _ in range(3)]

    for i in range(num_address):
        splits = []
        city = 'Barbarahaven'
        while len(splits) != 3:
            address = faker.address().replace('\n',',')
            splits = address.split(',')
            if not i in fixed_cities:
                city = splits[1]
        if not i in fixed_cities and randint(0, 15)==0:
            result.append({
                'street': '',
                'city': '',
                'postal_code': ''
            })
        else:
            result.append({
                'street': splits[0], 
                'city': city, 
                'postal_code': splits[2][1:],
            })
    # Insert mandatory address
    result.append({
        'street': '425 Phillips Pine', 
        'city': 'Dickersonburgh', 
        'postal_code': 'MI 59228',
    })
    return result
    
def generate_incidents(police_codes, addresses, suspects, agents):
    result = []
    faker = Faker()
    start = randint(1350,1440)
    john_doe = next((item for item in suspects if item["name"] == JOHN_NAME), None)
    # Insert mandatory city
    for i in range(NUM_INCIDENTS):
        # Generate timestamp
        timestamp = faker.date_time_between(start_date='-1w')
        # Pick a random police code
        police_code = police_codes[randint(0, len(police_codes)-1)]
        # Pick an address
        value = -1
        while value < 0 or value > len(addresses) - 1:
            value = gauss(len(addresses) / 2, len(addresses) / 4)
        address = addresses[int(value)]
        # Pick a suspect (or more)
        guilty = []
        num_subjects = randint(0, MAX_SUBJECT_PER_INCIDENT) 
        for _ in range(0, num_subjects): 
            guilty.append(suspects[randint(0, len(suspects)-1)])
        # Add John Doe to specific incident
        if start + i == JOHN_INCIDENT:
            guilty.append(john_doe)
        # Exclude John Doe from specific incident
        if start + i == NOT_JOHN_INCIDENT:
            found_john = next((item for item in guilty if item["name"] == JOHN_NAME), None)
            while found_john is not None:
                print("New suspect for "+str(NOT_JOHN_INCIDENT))
                guilty.clear()
                for _ in range(0, num_subjects): 
                    guilty.append(suspects[randint(0, len(suspects)-1)])
                found_john = next((item for item in guilty if item["name"] == JOHN_NAME), None)
        # Pick an agent (or more)
        num_agents = randint(1, MAX_AGENTS_PER_INCIDENT) 
        for _ in range(0, num_agents):
            agent = agents[randint(0, len(agents)-1)]
            for suspect in guilty or [{'name': '', 'gender': '', 'date_of_birth': ''}]:
                result.append({
                    'id': start + i, 
                    'time': timestamp,
                    'type_code': police_code['code'],
                    'type_description': police_code['description'],
                    'location_street': address['street'], 
                    'location_city': address['city'], 
                    'location_postal_code':  address['postal_code'],
                    'agent_name': agent['name'],
                    'agent_badge_number': agent['badge'],
                    'suspect_name': suspect['name'], 
                    'suspect_gender': suspect['gender'], 
                    'suspect_date_of_birth' : suspect['date_of_birth'],
                })
    return result

police_codes = read_police_codes()
addresses = generate_addresses()
suspects = generate_suspects()
agents = generate_agents()

incidents = generate_incidents(police_codes, addresses, suspects, agents)

with open('incidents.csv', mode='w') as csv_file:
    fieldnames = list(incidents[0].keys())
    writer = csv.DictWriter(csv_file, fieldnames=fieldnames)
    writer.writeheader()
    for incident in incidents:
        writer.writerow(incident)
