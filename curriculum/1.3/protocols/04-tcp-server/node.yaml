name: TCP & asyncio
description: Design a TCP-based protocol, and write asynchronous servers and clients for it.
goals:
    tcpudp: 1
    async: 1
    protocols: 1
days: 2
resources:
    Asynchronous programming:
        - |
            The concept of asynchronous programming can be tricky to wrap your head around. Here's what to do:

            - Take your time. If it takes the entire day, that's fine.
            - Watch/read multiple explanations. Each one will make a little more sense than the last, hopefully.
            - Experiment!
        -
            link: https://www.youtube.com/watch?v=fER1kP_2Ubg
            title: The Art of Software - An intro to async programming
            info: A video on the conceptual difference between synchronous programming, multi-threading programming and asynchronous programming.
        -
            link: https://www.youtube.com/watch?v=tRsRg2KQE54
            title: The Art of Software - A practical introduction to Python's asyncio
            info: Explains asyncio, without sockets. 
        -
            link: https://www.youtube.com/watch?v=tSLDcRkgTsY
            title: Python tricks - Demystifying async, await, and asyncio
            info: "This video covers the same ground as the previous one, but features sun glasses, a hat and a nice Dutch accent! :-)"
        -
            link: https://www.youtube.com/watch?v=h2IM-OPofqg
            title: Introduction to Python Asyncio for Beginners
            info: Again, the same concept, but now explained as a cook preparing multiple dishes concurrently.
        -
            link: https://medium.com/@esfoobar/python-asyncio-for-beginners-c181ab226598
            title: Python Asyncio for Beginners
            info: The same cook example, but this time explained in text instead of video.
        -
            link: https://docs.python.org/3/library/asyncio-task.html
            title: Python Documentation - asyncio - Coroutines and Tasks
            info: The official documentation, to be used as a reference.
        - |
            If you've understood the basics from the above resources, let's move on to using asynchronous programming where it really shines: networking!
        -
            link: https://docs.python.org/3/library/asyncio-stream.html
            title: Python Documentation - asyncio - Streams
            info: The official documentation for the high-level streams API. The examples at the bottom may be especially useful.

    Writing specifications:
        -
            link: https://www.youtube.com/watch?v=TtKKKUy4WU0
            title: Network Encyclopedia - What is Request For Comments (RFC)
        -
            link: https://redis.io/topics/protocol
            title: Redis Protocol specification
            info: Redis is a key/value database server using a relatively simple text-based protocol. This is the specification for that protocol. It is not an actual RFC, but a bit more informal (easier to read, but not as exact). You can use it as an example when writing your own protocol specification.

assignment:
    - |
        This assignment consists of three parts: designing a TCP-based chat protocol, implementing a server for it, and implementing a chat bot (client) for it.
    - Chat protocol:
        - |
            Write an informal specification (similar to the Redis example) for a chat protocol. The protocol should allow a user to...

            - Connect, providing a user name. The connection should be refused if the user name is already taken by a currently online client.
            - Join a chat channel. If the channel does not yet exist, it should be created. Other participants in that channel receive a user-joined-notification. If the user was participating in another channel he/she will leave that channel, and other channel participants should receive a user-left-notification.
            - Retrieve the list of users in the current channel.
            - Send messages, that will be received by everybody in the channel, together with the name of the sender. 
            - Disconnect. Participants of the active channel receive a user-left-notification. This should also happen when the TCP connection just ends, for example when a computer crashes or is not longer connected.

            Technical requirements:

            - The protocol should be text-based and easy for a user to control with just `nc` (netcat) or `telnet` as a client.
            - Messages should be able to contain any characters without confusing the protocol.
            - Don't forget to consider character encoding and port number.

            Write your specification in a `.txt` or a `.md` (Markdown) file. It should probably be in the 5kb to 10kb range.

            It's probably a good idea to write a very rough first draft of the protocol, then move on to actually implementing it, after which you can come back and finalize the protocol based on the practical experience you now have.

        -
            title: Protocol
            text: To what extend does the protocol meet the requirements? Does it do so in a simple/elegant way?
            1: Doesn't meet some requirement(s) and is pretty convoluted. 
            2: Doesn't meet some requirement(s) or is pretty convoluted.
            3: Meets almost all requirements and easy to understand.
            4: Meets all requirements and elegant!
        
        -
            title: Specification
            text: Is the specification of the protocol easy to understand? Does it read like an RFC?
            2: Some important aspects are left unanswered.
            3: Good, easy enough to understand.
            4: Great, including a visual representation!

    - Chat server:
        -
            ^merge: feature
            text: |
                Implement a server for your protocol, meeting all of the protocol's requirements. The server should keep things tidy: removing users from any lists when they disconnect, and deleting channels when they become empty.

                You should be able to test your server using multiple `nc` (or `telnet`) connections.

                *Hint:* use the examples in the Python asyncio Streams documentation to get started.
    
    - Scout bot:
        -
            ^merge: feature
            text: |
                Your server should be able to list which users are joined to a specific channel. There's no easy way of knowing the complete list of online users though. You, the creator of the server, don't want to implement a feature for that though. (Providing a long list of user names is probably not even AVG-compliant, right?)

                However an entrepreneurial user of the system, also enacted by you, comes up with the idea to create a chat bot (called *Scout*) that scouts the list of users. It should work like this:

                - It creates a connection, logging in with a random user name.
                - Every 3 seconds, it retrieves the list of channels. For every (new) channel:
                    - Create another connection, logging in with another random user name.
                    - Get the list of channel members.
                    - Monitor user-joined and user-left notifications.
                    - Maintain a list of all online users.
                    - When the bot is the only user left in the channel, it should disconnect, closing the channel.
                - Every 3 seconds, it should post the list of online users (from all channels) to a channel called `scout`. (It would be best if all the scout users were excluded from this list, but that's not a requirement.)

