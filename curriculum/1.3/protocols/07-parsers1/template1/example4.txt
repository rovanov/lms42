======================================== Tokens ========================================
'print' <identifier>
'(' <delimiter>
'"Welcome!"' <string>
')' <delimiter>
';' <delimiter>
'print' <identifier>
'(' <delimiter>
')' <delimiter>
';' <delimiter>
'var' <keyword>
'name' <identifier>
'=' <operator>
'input_str' <identifier>
'(' <delimiter>
'"Your name (or leave empty for default): "' <string>
')' <delimiter>
';' <delimiter>
'if' <keyword>
'(' <delimiter>
'name' <identifier>
')' <delimiter>
'{' <delimiter>
'}' <delimiter>
'else' <keyword>
'{' <delimiter>
'var' <keyword>
'name' <identifier>
'=' <operator>
'"world"' <string>
';' <delimiter>
'}' <delimiter>
'print' <identifier>
'(' <delimiter>
'"Hello"' <string>
',' <delimiter>
'name' <identifier>
',' <delimiter>
'"!"' <string>
')' <delimiter>
';' <delimiter>
'' <eof>

======================================== AST ========================================
tree.Program(
    block=tree.Block(
        statements=[
            tree.FunctionCall(
                name='print',
                arguments=[tree.Literal(value='Welcome!')]
            ),
            tree.FunctionCall(name='print', arguments=[]),
            tree.Declaration(
                identifier='name',
                expression=tree.FunctionCall(
                    name='input_str',
                    arguments=[
                        tree.Literal(
                            value='Your name (or leave empty for default): '
                        )
                    ]
                )
            ),
            tree.IfStatement(
                condition=tree.VariableReference(name='name'),
                yes=tree.Block(statements=[]),
                no=tree.Block(
                    statements=[
                        tree.Declaration(
                            identifier='name',
                            expression=tree.Literal(value='world')
                        )
                    ]
                )
            ),
            tree.FunctionCall(
                name='print',
                arguments=[
                    tree.Literal(value='Hello'),
                    tree.VariableReference(name='name'),
                    tree.Literal(value='!')
                ]
            )
        ]
    )
)

======================================== Running program ========================================
Welcome!

Your name (or leave empty for default): Hello world !
