from tree import *
import json
import sys


class Parser:

    def __init__(self, tokens):
        self.tokens = tokens
        self.current_token_pos = 0
        self.expected_tokens = [] # The list of things the current token did *not* match (used for error reporting).
    
    #
    # Utility functions, to be used by the actual parser functions below.
    # You should not need to change these, but you should understand what they do.
    #

    @property
    def token(self):
        """Get the current token. This is a @property, so use `self.token` instead of `self.token()`."""
        return self.tokens[self.current_token_pos]


    def next_token(self):
        """Progress to the next token."""
        self.expected_tokens = []
        self.current_token_pos += 1


    def match_kind(self, kind):
        """This is a utility function you may want to use instead of using `token` and `next_token()`
        directly. It returns the current token *if* it matches the given `kind`, or None otherwise.
        In case of a match, next_token() is called (but the original token is returned).
        In case of no match, the kind is added to `expected_tokens`, which helps `error` give
        a better message."""
        if self.token.kind==kind:
            token = self.token
            self.next_token()
            return token
        self.expected_tokens.append(f"<{kind}>")


    def match_text(self, text):
        """This is a utility function you may want to use instead of using `token` and `next_token()`
        directly. It returns the current token *if* it matches the given `text`, or None otherwise.
        `text` may also be a list of options to match.
        In case of a match, next_token() is called (but the original token is returned).
        In case of no match, the kind is added to `expected_tokens`, which helps `error` give
        a better message."""
        texts = text if isinstance(text,list) else [text]
        if self.token.text in texts:
            token = self.token
            self.next_token()
            return token
        self.expected_tokens += [repr(text) for text in texts]


    def error(self):
        """Display an error message, indicating the line, column, text and kind of the current token and 
        the list of tokens that were expected instead (from `self.expected_tokens`), and exits the program. 
        """
        print(f"Expected {' or '.join(self.expected_tokens)} at line {self.token.line} column {self.token.column}, but found {repr(self.token)}.")
        sys.exit(1)


    def require(self, val):
        """A utility function that may be convenient instead of checking for errors manually. It
        exits with an error if `val` is `None` or `False`, or just returns `val` otherwise.
        """
        if val==False or val==None:
            self.error()
        return val


    #
    # The actual parser functions.
    #

    def parse(self):
        """The main parse method. It returns a Program object, or exits with an error.
        It should be called only once on an instance."""
        block = self.parse_statements()
        # Make sure that after parsing all statements that we can, we have arrived at the
        # end-of-file marker. (If not, that would indicate code at the end.)
        self.require(self.match_kind('eof'))
        return Program(block)


    def parse_statements(self):
        """Parse a list of statements until the next thing that we encounter doesn't look
        like a statement. The list may be empty."""
        statements = []
        while True:
            statement = self.try_statement()
            if not statement:
                return Block(statements)
            statements.append(statement)


    def try_statement(self):
        """Optionally parse a single statement.
        Examples of statements are: '3 + 4;' and 'while(true) { print("loop"); }'
                
        All the `try_` functions like this should return a Tree node if it parsed successfully,
        return None if the token stream doesn't look like this type of node, or call `this.error`
        (possibly through `this.require`) in case the token stream *does* seem to represent this
        type of node, but something unexpected was encountered.
        """
        # We'll return the Tree node for the first statement type that matches.
        return self.try_expression_statement() or self.try_declaration() # TODO: add more statement types


    def try_expression_statement(self):
        """Optionally parse an expression statement, which is an expression followed by a ';'."""
        statement = self.try_expression()
        if statement:
            self.require(self.match_text(';'))
            return statement


    def try_declaration(self):
        """Optionally parse a 'var' declaration statement."""
        if self.match_text('var'):
            identifier = self.require(self.match_kind('identifier'))

            # TODO: parse optional initialization expression.
            # If the next token is an '=' it should be followed by an expression, which
            # the try_expression method will parse for you.
            expression = None

            self.require(self.match_text(';'))
            return Declaration(identifier, expression)


    def try_expression(self):
        """Optionally parse an expression."""

        # TODO: implement binary operators here

        return self.try_atom()


    def try_atom(self):
        """Optionally parse an atom. Atoms are parsing units at the bottom of the parse tree.
        They can be things like literals, variable references and function calls. But also a
        subexpression between parenthesis."""

        token = self.match_kind('string')
        if token:
            return Literal(json.loads(token.text))

        # TODO: more types of atoms


    # TODO: more parser functions!
