name: Lists
Description: Create a master/detail application using the `ListView` and static data.
goals:
    android-lists: 1
intro:
    About the exam: |
        <div class="notification">
            The exam for this module consists of a project that requires you to implement something based on your own idea. We recommend that you come up with an idea in advance. The project requirements are available at any time.
        </div>

resources:
    -
        link: https://www.youtube.com/watch?v=rdGpT1pIJlw
        title: Android Studio For Beginners Part 3
        info: The video uses RelativeLayout instead of ConstraintLayout, but these are quite similar so don't let that confuse you. Stop watching when he starts talking about image scaling.
        contents: with standard adapter, then custom adapter, onItemClickListener, adding an image, image scaling (skip!).
    -
        link: https://classroom.udacity.com/courses/ud839/lessons/abfcde44-3d89-437b-8d30-1f37f1d151fc/concepts/c29d1a15-ea54-4328-b8a3-930ba54ed7fd
        title: Udacity - Android Basics - Arrays, Lists, Loops & Custom Classes
        info: In case the video above is not for you, try to work your way through concepts 13 through 29 of this course. (A free Udacity account is required.)
        TODO: Does this include onItemClickListener? No.

assignment:
    - |
        Let's create a cocktail recipe app! It allows users to maintain a list of cocktails, each having a name, color and mixing instructions. We'll extend this app in the next two lessons.

    -
        ^merge: feature
        code: 0.25
        text: |
            The first step is to add a `FloatingActionButton` with an *add* icon on the otherwise empty `MainActivity`. Tapping it should open a new activity where the user can add details about a cocktail. These are the details a user should be able to input:

            - Name.
            - Instructions. A multi-line text input `View` should be used for this.
            
            The `Activity` should also have a *Save* button, that creates a new *Recipe* object, appends it to the list, and closes the current activity (`finish()`). As we have yet to learn how to use a proper database in Android, for now you can 'store' the list as a `public static` variable of a class created specifically for this (`Data` seems like a suitable name for the class). That basically means the data is stored in a global variable, so it will disappear when the app is closed. We'll fix that in later lessons!

            The user should be able to navigate *up* (using the arrow in the `ActionBar`). When doing so (or when going *back*), the new recipe will not be saved.

    - 
        ^merge: feature
        weight: 1.5
        code: 0.25
        text: |
            Within your `MainActivity`, display the list of cocktails using a `ListView`. We recommend that you start by using a standard `ArrayAdapter` and a built-in layout (such as `android.R.layout.simple_list_item_1`).

            Tapping on an item should open that cocktail in the edit `Activity` you created in the previous objective (using `setOnItemClickListener`). You can use `Intent` *extra* to pass along which cocktail to edit. The edit `Activity` should of course be modified to load the right data, and to update instead of append when the `Save` button is pressed. Adding new cocktails (through the `FloatingActionButton`) should of course still work as well.

            When creating a new item, the `ActionBar` title should be *New cocktail*. When editing an item, the title should be the original name of the cocktail.

    -
        ^merge: feature
        code: 0.25
        text: |
            <img src="list-example.png" class="side">We want the app to look a bit fancier. In the list of cocktails, show a photo next to each cocktail name. Images for seven differently colored cocktails have been provided. Select the most appropriate image based on the cocktail name. The `ImageView`s should be `64dp` wide and `64dp` high, and the right `scaleType` should be set to have the image fill up available space.

            The end result should resemble the screenshot.

