- Introduction: |
    In this chapter, you'll get familiar with the concept of "containers". You will look at what containers are, how you can use them and finally how to create containers for your own application. The objectives are structured in such a way that you'll be guided from introduction to final realization, where the need to "experiment" increases over time. Therefore it is **highly** recommended that you follow our steps and confirm that you understand each step before proceeding.

- Tasks:
    - Install docker: |
        As Docker has an official Manjaro package, you can install it using *Add/remove software*. The package is called `docker`. Install this package now before you proceed.

        To verify that docker has been installed correctly run the following command:

        ```sh
        $ sudo docker version
        ```

        The first thing you will notice is that there will be an error, stating *Cannot connect to Docker daemon*: This is because the Docker Engine hasn't actually started yet.
        There should be some information about the *client*. You will use this client to instruct the engine *what to do*. The engine itself is a background process.

        Start the Docker Engine by running.
        ```sh
        $ sudo systemctl start docker.service
        ```
        Note that starting the docker service does not mean that it will automatically start after a reboot. If you want to automatically start the docker service after a reboot you should 'enable' it as follows:

        ```sh
        $ sudo systemctl enable docker.service
        ```

        In order to work with the docker service, you'll need permission to do so. Add yourself to the `docker` group using this command:

        ```sh
        $ sudo usermod -aG docker $USER
        ```

        Note that you should restart your console at this point as changes in group are not immediately reflected in a running shell. (Alternatively you can reboot your system.)

        Now confirm that the engine has started by repeating the first command (`docker version`) before doing the check below.

    - Verify with "Hello Docker": |
        To verify that your installation was a success, run the "hello-world" docker image and study it's output. A docker image contains the files needed to run a particular application. Running the docker image in a docker container will execute the application stored in the image. 
        
        To launch a new container with the "hello-world" image, run:
        ```
        $ docker run hello-world
        ```

        As you can see, the required image will be downloaded automatically and a container is started. The output should look something like:

        ```
        Hello from Docker!
        This message shows that your installation appears to be working correctly.
        ...
        ```

        If you see output above it means docker is correctly installed on your laptop. If not, please ask your mentor or teacher for help.


- Assignment:
    # - Introduction: 
    #     -
    #         link: https://youtu.be/JSLpG_spOBM
    #         title: Introduction To Docker and Docker Containers
    #         info: A short history on virtualization and containerization.
    #     - |
    #         Before you start, please watch the video linked to this introduction. This will give you an overview of what the Docker is. 

    #         The most *basic* usage for Docker is to use containers to host *services*. A *service* is some kind of application that might consist of one or multiple parts (e.g. some html pages (website) or a Flask backend using a postgres database).

    #         When others want to deploy your service (or as you might know it: *install it*), it requires knowledge of how you build your application. Does it require a database, what OS should be used, what applications are used to run the actual code (Java? Python? Rust? etc.), what dependencies are required, etc. etc. As you can imagine, deploying a service usually requires *effort*. The idea behind docker is to minimize this deployment effort using prepackaged containers (as explained in the video).

    - Primer:
        -
            link: https://automateinfra.com/2021/04/11/how-to-create-dockerfile-step-by-step-and-top-5-dockerfile-examples/
            title: How to Create Dockerfile step by step and Build Docker Images using Dockerfile
            info: Explains how to create custom Docker images with a Dockerfile
        -
            text: |
                At this point you should have docker installed and should have run your first ("Hello World") container. Before we dive deeper into Docker we want to have a look at how this "Hello World" container works. Containers are running **Docker Images**. For our "Hello World" container we have used an image that was already created. This image can be found on [Docker Hub](https://hub.docker.com/_/hello-world/).
                
                Where does the image come from? This image was build using a `Dockerfile` and then uploaded to Docker Hub for everyone to use. The process from Dockerfile to a running container is shown below. 

                <img src="docker-process.png" class="no-border">

                The purpose of this objective it to create and run our own "Hello World" container. In the `0-primer` folder you will find a `Dockerfile` and `analysis.txt` file. Have a look at the Dockerfile and try to understand what everything means. Then open the `analysis.txt` and follow the instructions.
            ^merge: feature

    - Your first container:
        - 
            text: |
                Let's build a Docker image that can run a static website. In the template folder you will find a folder called `1-container`. It contains a simple static website that you can run using the provided script (`start.sh`). The script also logs the requests to a file called `log.txt`. Run the script, visit the website (on port 9000) and validate that the log file is created and contains the requests.
                
                Your task is to finish the Dockerfile so that:
                1. the container should have `/app` as working directory
                2. copy the required files to the working directory
                3. last but not least you should run the `start.sh` script using the [CMD](https://docs.docker.com/engine/reference/builder/#cmd) command to serve the website.

                Run your container and validate it by opening the following url in your browser `http://localhost:9000` (you should see the static website).
            must: true

    - Script your container management:
        - 
            link: https://www.freecodecamp.org/news/docker-detached-mode-explained/
            title: Docker detached mode
            info: The option --detach or -d, means that a Docker container runs in the background of your terminal. 
        - 
            text: |
                When you run your container from the terminal the information from the container is displayed in your terminal. This is because the container is running in the foreground. By running the container in detached mode you can move this to the background. 

                In the template folder you will find a `todo.sh` script. Edit the script so that when you run `./todo.sh 1` it will build and run the container in detached mode and with a proper name.  
            ^merge: feature
                 
    - Container diagnostics:
        -
            link: https://www.baeldung.com/ops/docker-container-shell
            title: Getting Into a Docker Container’s Shell
            info: A small tutorial that explains how to run an interactive shell in the container.
        - 
            text: |
                Sometimes your container does not behave like it should. In such cases it is useful to 'connect' to your container (running a terminal) and see what is happening inside. Note that the container is a basic linux environment. So most linux commands can be used. 
                
                Currently your container logs the requests to the static website. Your task is to:
                1. shell to the container (run it if it is not running yet :)
                2. locate the `log.txt` in the container
                3. copy its contents (use the linux `cat` command) to the `1-container/container.log` file in your template folder.

                *Tip*: With `docker ps` you can see your running containers. 
            must: true

    - Run existing images:
        -
            link: https://www.youtube.com/watch?v=p2PH_YPCsis
            title: Docker Volumes explained in 6 minutes
            info: In this video the concept of Docker volumes is explained. Section after 4:14 (on docker composed is not required at this point).
        -
            link: https://youtu.be/xGn7cFR3ARU?t=608
            title: 8 Basic Docker Commands (docker ports, docker port mapping)
            info: In this video the concept of Docker ports and port mapping is explained. 
        -
            text: |
                Aside from building your own images you can also leverage existing images. For example, [nginx](https://www.nginx.com) (a frequently used web server application) is also available as a Docker image, also [see](https://hub.docker.com/_/nginx/). Give it a try by running `docker run --name my-nginx nginx`. What do you see when you open your browser on `http://localhost`?

                Your task is to use serve the static website using nginx. You should:
                1. find out where the nginx's default folder for serving static content is located
                2. create a volume mapping to this folder
                3. create a port mapping so that the website is served on port 8080 (the website should be shown when you go to `http://localhost:8080`)
                4. edit the script so that when you run `./todo.sh 2` it will serve serve the static website using nginx.
            ^merge: feature

    - Reverse proxy:
        - 
            link: https://docs.nginx.com/nginx/admin-guide/web-server/reverse-proxy/
            title: NGINX Reverse Proxy
            info: Explains how to configure nginx as a reverse proxy (carefully read the section "Passing a Request to a Proxied Server")
        -
            text: |
                A reverse proxy is a server that forwards client requests to another web server and serves the response back to the client. Have a look a the provided resource to understand how this can be done using nginx.

                Your task is to configure the default nginx container as a reverse proxy. You should:
                1. locate and study the nginx configuration file (note that the nginx.conf does not need to be edited)
                2. create a volume mapping between the configuration file provided in the template (see `2-reverse-proxy`)
                3. edit the `2-reverse-proxy/reverse_proxy.conf` file so that request to 'http://localhost:8080/search' are forwarded to google.com
                4. edit the script so that when you run `./todo.sh 3` it will serve serve the static website using nginx and reverse proxy requests to /search to google.com. You may reuse your script from the previous object.
            must: true

    - Docker compose:
        - 
            link: https://docs.docker.com/compose/gettingstarted/
            title: Get started with Docker Compose
            info: This tutorial explains how to create a simple web application using Docker compose.
        -
            text: |
                Finally we have Docker compose which can be used to run multiple containers. With Docker compose you can configure which images to build and containers to run in one single file (called `docker-compose.yml`).

                Your task is to edit the `docker-compose.yml` in the `3-compose` folder so that a standard nginx container is started with:
                1. a volume mapping to the static website
                2. and a port mapping to port 8080 (making the website reachable from `http://localhost:8080`).

                When you run `docker compose up` the website should be displayed.
            must: true 

    - Bring it all together:
        -
            text: |
                In this objective we will bring all aspects from the previous objects together into one objective. Your task will be to run a frontend and backend application in separate containers (using docker compose) as shown in the image below. 
                
                <img src="docker-custom-container-multiple.png" class="no-border">
                
                You should edit the `todo.sh` so that running `./todo.sh 4` will do everything that is required to run the application when running `docker compose up` (which should be the final command in the script). Your script should take the following requirements into account:
                
                1. For your frontend container:
                    - The code can be found [here](https://gitlab.com/sealy/simple-todo-app). In your script you should clone this repository to the `4-complete/frontend` folder.
                    - The frontend needs to be build before it can be served. When you run `npm build` a distribution is created in a folder called `dist`. The contents of this `dist` folder can be served using nginx.
                    - The frontend should be served by a web server container (nginx) mapped to the host computer on TCP port 80.
                    - Use a volume mapping to serve a distribution of the frontend app.
                    - The frontend web server should be configured as a reverse proxy that redirects request to [http://localhost/todos](http://localhost/todos) to the backend container. The name you give the container in your docker-compose file, can be used as a hostname within your nginx configuration file (just like the `redis` host can be used from within `app.py` in the *Get started with Docker Compose* resource).
                2. For your backend container:
                    - The backend code can be found [here](https://gitlab.com/sealy/simple-todo-backend). In your script you should clone this repository to the `4-complete/backend` folder.
                    - The backend is a Flask application that can be run using poetry.
                    - Create a Docker image for running this backend. A simple python base image should be sufficient for running the app.                
            ^merge: feature
            weight: 1.5

    - Clean up:
        -
            link: https://linuxhandbook.com/remove-docker-images/
            title: Complete Guide for Removing Docker Images
            info: Explains the various ways to remove a docker image
        -
            text: |
                Implement the clean up script (`./todo.sh 5`). It should:
                1. Stop all your containers (so other containers should not be stopped)
                2. Remove your images (please leave the other images alone)
            must: true




    # - Building containers:
    #     -
    #         link: https://automateinfra.com/2021/04/11/how-to-create-dockerfile-step-by-step-and-top-5-dockerfile-examples/
    #         title: How to Create Dockerfile step by step and Build Docker Images using Dockerfile
    #         info: Explains how to create custom Docker images with a Dockerfile
    #     -
    #         link: https://www.whitesourcesoftware.com/free-developer-tools/blog/docker-expose-port/
    #         title: How to Expose Ports in Docker
    #         info: Explains how Docker ports work in order to make a container available to the outside world. 
    #     -
    #         text: |
    #             In the primer we have practiced with building and running a container from a Dockerfile. In this objective you will create a custom Dockerfile to build and run a static website. In the `1-container` template folder you will find the required files for the website. To run the website simply run the provided `script.sh`.

    #             Your task is to:
    #             - edit the Dockerfile in such a way that all required files are added to the container image.
    #             - run the script in the container when it is started (using the CMD command).
    #             - expose the proper port so that the website is available to the host machine. 

    #             Tips (because tips are awesome):
    #             - When you run a container using the script it will stay running until you stop it (by pressing Ctrl-C). In order to run the container in the background (so that you can use your terminal for other stuff) you will need to run the container in Daemon mode (using the -d option `docker run -d ... `).
    #             - Containers running in Daemon mode can be stopped using the `docker stop <container_id>` command. In order to find the container id run `docker ps` to get a list of running containers. Pro tip: you do not need to copy the entire container id you can use the first couple (for example two) characters as long as they are unique in the list of running containers.

    #         ^merge: feature

    # - Run a static website using Docker:
    #     - 
    #         text: |
    #             Install some website in the container and run it.
    #             Next step would be to host it with an off the shelf container.

    #             The application we want to run in this container is a simple todo app. The todo app can be found [here](https://gitlab.com/sealy/simple-todo-app). You can run this app on your laptop by cloning the repository and running the following commands:
    #             ```sh
    #             $ npm install
    #             $ npm run serve
    #             ```
    #             Note that this command `npm run serve` runs a development server which hosts the todo application which is useful for faster development. If you run the app using a web server (which is our case) then this command is not needed. There is also a command `npm run build` which creates a `dist` folder containing a static version of the app. 

    #             Your task is to extend the **todo.sh** script to include a new option (for example by running "./todo.sh nginx") for cloning the repository and building web application and the Dockerfile. The script should run the container afterwards. The Dockerfile should:
    #             - include a distribution of the static website in the container (so you should copy the distribution to the container)
    #             - serve the static website using nginx
    #             - expose the correct port (should be port 80 this time) in the docker container. 
                
    #             In your script you should include a mapping between the port in the container running the website to port 80 (if successful you should see the todo app when navigating to [http://localhost](http://localhost) in your browser). Also be sure to name your container (with the --name flag).

    #             Note that if you custom container does not behave as you would expect you should open a shell to the running container to further analyse the problem.
    #         ^merge: feature


    # - Using an off the shelf image:
    #     -
    #         link: https://www.youtube.com/watch?v=p2PH_YPCsis
    #         title: Docker Volumes explained in 6 minutes
    #         info: In this video the concept of Docker volumes is explained. Section after 4:14 (on docker composed is not required at this point).
    #     -
    #         text: |
    #             <wired-card>
    #                 <h1 style="width: 600px;">Hello Docker!</h1>
    #                 <wired-divider></wired-divider>
    #                 <div style="display: flex; flex-wrap: wrap;">
    #                     <div style="flex: 50%;">
    #                         <p>This is the welcome page for my first web app running on Docker!</p>
    #                     </div>
    #                     <div style="flex: 50%;">
    #                         <wired-card>
    #                             <wired-image src="docker-logo.png"></wired-image>
    #                         </wired-card>
    #                     </div> 
    #                 </div>
    #             </wired-card>

    #             Static websites are served by a web server like for example nginx. In order to serve a static website using nginx you would have to install it on your machine and configure is so it serves the correct HTML file(s). Instead of installing nginx we can also run it as a container. Using a volume mapping we can then configure nginx to serve our own static website.  

    #             Your tasks:
    #             - Implement the provided wireframe in the *index.html* file provided in the **1-volume_mapping** folder.
    #             - Write a script (in **todo.sh**) that runs an off the shelf nginx container and serves your index.html. Your application should be accessible via port 8080, so opening [http://localhost:8080](http://localhost:8080) in your browser should show the web page. You can find the off the shelf nginx container on [Docker Hub](https://hub.docker.com). Don't forget to configure the volume mapping and port mapping for your container.

    #             Note: you should give your container a name so that you can easily find it later on.
    #         ^merge: feature

    # - Create a custom 404:
    #     -
    #         link: https://linuxize.com/post/how-to-list-docker-containers/
    #         title: How to List Containers in Docker
    #         info: Describes how to get a list of (running) containers.
    #     - 
    #         link: https://linuxize.com/post/how-to-connect-to-docker-container/#get-a-shell-to-a-container
    #         title: How to Connect to a Docker Container
    #         info: Describes different ways to connect to a running container.
    #     -
    #         text: |
    #             The container is running a standard configuration of nginx. This means that if you navigate to http://localhost:8080/123abc](http://localhost:8080/123abc) you will get the standard nginx 404 page. We want to change this to show our own custom 404 page.

    #             Your tasks:
    #             - Create a *404.html* file in the **1-volume_mapping** folder and write your own 404 page.
    #             - Open a shell on the running container and change the nginx configuration (located in /etc/nginx/conf.d/default.conf) so that it points to your 404 page in case of a 404.
    #         ^merge: feature

    # - Dockerize the todo app:
    #     - 
    #         link: https://linuxize.com/post/how-to-connect-to-docker-container/
    #         title: How to Connect to a Docker Container
    #         info: Describes different ways to connect to a running container.
    #     -
    #         link: https://linuxhandbook.com/remove-docker-images/
    #         title: Complete Guide for Removing Docker Images
    #         info: Explains the various ways to remove a docker image
    #     - 
    #         text: |
    #             A different way to run the application is to create a custom container. In order to do so you have to create a Dockerfile and build a container based on it. For this objective you should create a container based on a nginx container (your base image).
                
    #             The application we want to run in this container is a simple todo app. The todo app can be found [here](https://gitlab.com/sealy/simple-todo-app). You can run this app on your laptop by cloning the repository and running the following commands:
    #             ```sh
    #             $ npm install
    #             $ npm run serve
    #             ```
    #             Note that this command `npm run serve` runs a development server which hosts the todo application which is useful for faster development. If you run the app using a web server (which is our case) then this command is not needed. There is also a command `npm run build` which creates a `dist` folder containing a static version of the app. 

    #             Your task is to extend the **todo.sh** script to include a new option (for example by running "./todo.sh nginx") for cloning the repository and building web application and the Dockerfile. The script should run the container afterwards. The Dockerfile should:
    #             - include a distribution of the static website in the container (so you should copy the distribution to the container)
    #             - serve the static website using nginx
    #             - expose the correct port (should be port 80 this time) in the docker container. 
                
    #             In your script you should include a mapping between the port in the container running the website to port 80 (if successful you should see the todo app when navigating to [http://localhost](http://localhost) in your browser). Also be sure to name your container (with the --name flag).

    #             Note that if you custom container does not behave as you would expect you should open a shell to the running container to further analyse the problem.
    #         ^merge: feature

    # - Dockerize the todo app:
    #     -
    #         text: |
                
    # - Push your container to a registry:
    #     -
    #         link: https://youtu.be/pTFZFxd4hOI?t=931
    #         title: Docker Tutorial for Beginners [2021]
    #         info: A tutorial explaining the Docker workflow (watch until about 17:45)
    #     -
    #         link: https://docs.docker.com/docker-hub/repos/
    #         title: Repositories
    #         info: A tutorial explaining how to create, push and pull container registries on Docker Hub.
    #     - 
    #         text: |
    #             As explained in the video containers are distributed using container registries. Your objective is to push the container image you have created in the previous objective. Docker Hub is a popular Docker registry for which you can create an account en store your own registries. 
                
    #             Do the following:
    #             - Create an account on Docker Hub for storing your container image.
    #             - Extend your script (for example "./todo.sh push") to build the nginx container and to push it to your own repository on Docker Hub. Your script should also remove the existing containers/images and run the container. Running this script should show that the image is pulled from Docker hub before running the container.
    #         ^merge: feature
    #         weight: 0.5

    # - Custom Docker image:
    #     - 
    #         link: https://automateinfra.com/2021/04/11/how-to-create-dockerfile-step-by-step-and-top-5-dockerfile-examples/
    #         title: How to Create Dockerfile step by step and Build Docker Images using Dockerfile
    #         info: Explains how to create custom Docker images with a Dockerfile
    #     - 
    #         text: |
    #             Currently our app resets the state of the todo app every time we refresh the browser. We want to include a backend to save the state of the application. For this we will use a simple backend which can be found [here](https://gitlab.com/sealy/simple-todo-backend). In order to allow the frontend to connect to the backend you should checkout the **backend-connection** branch of the frontend (the simple todo app). In this version of the frontend the todos are retrieved from and stored in the backend (also see [this line of code](https://gitlab.com/sealy/simple-todo-app/-/blob/backend-connection/src/components/TodoList.vue#L34)).

    #             The end result of your custom container should look something like the picture below. You should have one container running both the backend and frontend.

    #             <img src="docker-custom-container.png" class="no-border">
                
    #             The purpose of this objective is to create a Docker file with both the frontend and the backend. As long as the backend is running the state of is maintained. You can check this by updating the todos and opening a new browser window to see whether the changes have been stored correctly. 
                
    #             You should:
    #             - Extend the current Dockerfile in the 2-todo-complete folder
    #             - Extend you script to build this new Dockerfile (for example "./todo complete")

    #             In your Dockerfile you should:
    #             - Install the required packages (for example nginx, python, etc.)
    #             - Copy the required application into the container (frontend and backend)
    #             - Expose the proper ports (use port 8080 for the frontend (nginx) and port 80 for the backend)
    #             - Start the webserver and the backend application (use a script to achieve this). 

    #             Note that you it would be wise to start with a local working setup with the frontend that connects to the backend before containerizing you solution. This way you can better see how the front- and backend work together.
    #         ^merge: feature
    #         weight: 1.5

    # - Docker compose:
    #     - 
    #         link: https://docs.docker.com/compose/gettingstarted/
    #         title: Get started with Docker Compose
    #         info: This tutorial explains how to create a simple web application using Docker compose.
    #     -
    #         link: https://docs.nginx.com/nginx/admin-guide/web-server/reverse-proxy/
    #         title: NGINX Reverse Proxy
    #         info: An explanation on how to configure NGINX as a reverse proxy.
    #     -
    #         text: |
    #             In the previous objective we have build a single container with the frontend and backend application. Containers should be single purpose and this is not really the way we build containers. In this objective we will run the frontend and backend in separate containers. Your assignment is to create these two separate containers as shown in the image below.
                
    #             <img src="docker-custom-container-multiple.png" class="no-border">

    #             For your frontend container:
    #             - The static frontend should be served by a standard web server (apache or nginx) mapped to the host computer on TCP port 80.
    #             - Use a volume mapping to serve a distribution of the frontend app.
    #             - The backend server should also run on port 80 in its own container, but the port should not be mapped to the host computer.
    #             - The frontend web server should be configured as a reverse proxy that redirects request to [http://localhost/todos](http://localhost/todos) to the backend container. The name you give a container in your docker-compose file, can be used as a hostname within your nginx configuration file. (Just like the `redis` host can be used from within `app.py` in the *Get started with Docker Compose* resource.)

    #             For your backend container:
    #             - The backend code should be included and run using poetry.
    #             - A simple python base image should be sufficient for running the app.                
    #         ^merge: feature
    #         weight: 1.5